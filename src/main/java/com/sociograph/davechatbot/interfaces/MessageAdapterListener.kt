package com.sociograph.davechatbot.interfaces

import androidx.fragment.app.FragmentManager
import com.sociograph.davechatbot.databinding.RFormFileInputViewBinding
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.FormItem
import com.sociograph.davechatbot.domain.resmodels.ThumbImageItem

interface MessageAdapterListener {
    fun onOptionClick(state: String, option: String)
    fun onSubmitForm(id: Long, customerState: String?, item: List<FormItem>)
    fun onImageClick(item: ThumbImageItem)
    fun onChooseFileClick(
        bind: RFormFileInputViewBinding,
        bindingAdapterPosition: Int,
        conversationItem: ConversationRes
    )

    fun getFilledConversationData(responseId:String): ConversationRes?
    fun setFilledConversationData(conversationRes: ConversationRes)
    fun onSubmitFeedback(responseId: String, response: String)
    fun getFragmentManager(): FragmentManager
}