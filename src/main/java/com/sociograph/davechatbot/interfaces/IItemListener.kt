package com.sociograph.davechatbot.interfaces

interface IItemListener<T> {
    fun onItemClick(position: Int, item: T)
}