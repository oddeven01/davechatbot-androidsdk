package com.sociograph.davechatbot.data.webservices

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.sociograph.davechatbot.BuildConfig
import com.sociograph.davechatbot.core.ConfigData
import com.sociograph.davechatbot.data.support_data.webservices.APIExceptionMapper
import com.sociograph.davechatbot.data.support_data.webservices.RxErrorHandlingCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit


internal class RestService private constructor() {

    companion object {

        private const val CONNECTION_TIMEOUT_SEC = 120
        private const val WRITE_TIMEOUT_SEC = 120
        private const val READ_TIMEOUT_SEC = 120

        private var instance: RestService? = null

        fun getInstance(): RestService {
            if (instance == null) {
                instance =
                    RestService()
            }
            return instance as RestService
        }
    }

    private var service: IService

    private var okHttpClient = getUnsafeOkHttpClient()

    private fun getOkHttpClient(): OkHttpClient {
        var builder = OkHttpClient.Builder()
        builder.interceptors().addAll(getInterceptorList())
        if (BuildConfig.DEBUG) {
            builder = builder.addNetworkInterceptor(StethoInterceptor())
        }

        builder = builder.connectTimeout(CONNECTION_TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
        builder = builder.writeTimeout(WRITE_TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
        builder = builder.readTimeout(READ_TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)

        return builder.build()
    }

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(ConfigData.BASE_API_URL)
            .client(okHttpClient.build())
            .addConverterFactory(GsonConverterFactory.create(GsonUtils.getInstance().gson))
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create(APIExceptionMapper()))
            .build()

        service = retrofit.create(IService::class.java)
    }

    private fun getInterceptorList(): ArrayList<Interceptor> {

        val interceptorList = ArrayList<Interceptor>()

        val loggingInterceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
        }
        interceptorList.add(loggingInterceptor)
        return interceptorList
    }

    fun getIService(): IService {
        return getInstance().service
    }

    private fun getUnsafeOkHttpClient(): OkHttpClient.Builder {
        try {

            var builder = OkHttpClient.Builder()
            builder.interceptors().addAll(getInterceptorList())
            if (BuildConfig.DEBUG) {
                builder = builder.addNetworkInterceptor(StethoInterceptor())
                //builder.addInterceptor(ChuckerInterceptor(HowzItApp.appContext))
            }

            //builder.addInterceptor(BasicAuthInterceptor(Injection.provideAppSettingDataSource()))
            //builder.addInterceptor(TokenInterceptor())

            builder = builder.connectTimeout(CONNECTION_TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
            builder = builder.writeTimeout(WRITE_TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
            builder = builder.readTimeout(READ_TIMEOUT_SEC.toLong(), TimeUnit.SECONDS)
            return builder
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}
