package com.sociograph.davechatbot.data.webservices

import com.google.gson.JsonElement
import com.sociograph.davechatbot.domain.resmodels.*
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*

@JvmSuppressWildcards
internal interface IService {

    /*@Headers("SkipAccessTokenHeader: true")
    @FormUrlEncoded
    @POST("oauth/token")
    fun getAuthToken(
        @Field("username") userName: String, @Field("password") password: String,
        @Field("client_id") clientId: String, @Field("client_secret") clientSecret: String,
        @Field("grant_type") grantType: String
    ): Single<AuthTokenRes>*/

    @POST("dave/oauth")
    fun login(@Body params: Map<String, Any>): Single<LoginRes>

    @POST("conversation/{conversationId}/{daveUserId}")
    fun conversation(
        @Path("conversationId") conversationId: String,
        @Path("daveUserId") daveUserId: String,
        @Body params: Map<String, Any?>,
        @HeaderMap headerParam: Map<String, String>
    ): Single<ConversationRes>

    @GET("conversation-history/{conversationId}/{daveUserId}")
    fun conversationHistory(
        @Path("conversationId") conversationId: String,
        @Path("daveUserId") daveUserId: String,
        @HeaderMap headerParam: Map<String, String>
    ): Single<ConversationHistoryRes>

    @GET("conversation-history/{conversationId}/{daveUserId}")
    fun conversationHistory(
        @Path("conversationId") conversationId: String,
        @Path("daveUserId") daveUserId: String,
        @Query("_page_size") pageSize: Int,
        @Query("_page_number") pageNumber: Int,
        @HeaderMap headerParam: Map<String, String>
    ): Single<ConversationHistoryRes>


    @GET("conversation-keywords/{conversationId}")
    fun conversationKeywords(
        @Path("conversationId") conversationId: String,
        @HeaderMap headerParam: Map<String, String>
    ): Single<ConversationKeywordRes>

    @Multipart
    @POST("upload_file")
    fun uploadFile(
        @Query("large_file") largeFile: Boolean,
        @Part file: MultipartBody.Part,
        @HeaderMap headerParam: Map<String, String>
    ): Single<UploadFileRes>


    @PATCH("conversation-feedback/dave/{engagement_id}")
    fun conversationFeedback(
        @Path("engagement_id") engagementId: String,
        @Body params: Map<String, Any?>,
        @HeaderMap headerParam: Map<String, String>
    ): Single<JsonElement>

    @PATCH("conversation-feedback/dave/{engagement_id}/{response_id}")
    fun conversationFeedback(
        @Path("engagement_id") engagementId: String,
        @Path("response_id") responseId: String,
        @Body params: Map<String, Any?>,
        @HeaderMap headerParam: Map<String, String>
    ): Single<JsonElement>

}
