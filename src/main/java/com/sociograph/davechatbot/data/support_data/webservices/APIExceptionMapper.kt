package com.sociograph.davechatbot.data.support_data.webservices

import com.google.gson.JsonParser
import com.sociograph.davechatbot.domain.support_domain.IAPIExceptionMapper
import com.sociograph.davechatbot.domain.support_domain.models.AppException
import com.sociograph.davechatbot.domain.support_domain.models.MError
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

internal class APIExceptionMapper :
    IAPIExceptionMapper {

    override fun decodeHttpException(exception: HttpException): Throwable {
        val appException = AppException()
        appException.kind = AppException.Kind.REST_API

        appException.originalException = exception

        try {
            val response = exception.response()
            val errorBody = response?.errorBody()!!.string()
            val httpCode = response.code()

            val jsonObject = JsonParser.parseString(errorBody).asJsonObject

            val decodedError = MError()
            decodedError.code = httpCode.toString()
            decodedError.name = response.message()
            decodedError.errorBody = errorBody

            decodedError.httpCode = httpCode
            appException.error = decodedError
            if (jsonObject.has("error") && jsonObject.get("error") != null) {
                decodedError.message = jsonObject.get("error").asString
            }

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return appException
    }

    override fun decodeUnexpectedException(throwable: Throwable): Throwable {

        val htException = AppException()
        htException.kind = AppException.Kind.UNEXPECTED
        htException.originalException = throwable
        return htException
    }

    override fun decodeIOException(ioException: IOException): Throwable {

        val appException = AppException()
        appException.kind = AppException.Kind.NETWORK

        /* dont send connectivity problem when there is time out */
        if (ioException is SocketTimeoutException) {
            appException.isNetworkTimeout = true
        } else {
            appException.originalException = ioException
        }
        return appException
    }
}