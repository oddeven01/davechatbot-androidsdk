package com.sociograph.davechatbot.data.repositaries

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.paging.*
import com.google.gson.JsonElement
import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.datasources.local.ILocalDataSource
import com.sociograph.davechatbot.domain.datasources.remote.IRestDataSource
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.ext.GenericRemoteMediator
import com.sociograph.davechatbot.domain.resmodels.*
import com.sociograph.davechatbot.domain.usecase.GetHistoryV1UseCase
import io.reactivex.Completable
import io.reactivex.Single

internal class AppDataRepository(
    private val appSettingDataSource: IAppSettingsDataSource,
    private val restDataSource: IRestDataSource,
    private val localDataSource: ILocalDataSource,
) : BaseRepository(), IAppDataSource {

    companion object {

        private var INSTANCE: AppDataRepository? = null

        @JvmStatic
        fun getInstance(
            appSettingDataSource: IAppSettingsDataSource,
            restDataSource: IRestDataSource,
            localDataSource: ILocalDataSource
        ): AppDataRepository {
            if (INSTANCE == null) {
                synchronized(AppDataRepository::javaClass) {
                    INSTANCE =
                        AppDataRepository(
                            appSettingDataSource,
                            restDataSource,
                            localDataSource
                        )
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }

    override fun login(
        userName: String,
        password: String,
        roles: String,
        attrs: List<String>,
        enterPriseId: String
    ): Single<LoginRes> {
        return restDataSource.login(userName, password, roles, attrs, enterPriseId)
    }

    override fun conversation(
        conversationId: String,
        daveUserId: String,
        customerResponse: String?,
        customerState: String?,
        engagementId: String?,
        systemResponse: String?,
        queryType: String?
    ): Single<ConversationRes> {
        return restDataSource.conversation(
            conversationId,
            daveUserId,
            customerResponse,
            customerState,
            engagementId,
            systemResponse,
            queryType
        )
    }

    override fun getConversationHistory(
        conversationId: String,
    ): Single<ConversationHistoryRes> {
        return restDataSource.getConversationHistory(conversationId)
    }

    override fun getConversationHistory(
        conversationId: String,
        pageSize: Int,
        pageNumber: Int
    ): Single<ConversationHistoryRes> {
        return restDataSource.getConversationHistory(conversationId, pageSize, pageNumber)
    }

    override fun getConversationKeywords(conversationId: String): Single<ConversationKeywordRes> {
        return restDataSource.getConversationKeywords(conversationId)
    }

    override fun insertMessage(userRes: UserRes): Long {
        return localDataSource.insertMessage(userRes)
    }

    override fun updateResponseById(id: Long, userRes: UserRes) {
        localDataSource.updateResponseById(id, userRes)
    }

    override fun getMessageCount(): Int {
        return localDataSource.getMessageCount()
    }

    override fun getSystemMessageCount(): Int {
        return localDataSource.getSystemMessageCount()
    }

    override fun insertAllMessageItem(messageItemList: List<MessageItemEntity>) {
        localDataSource.insertAllMessageItem(messageItemList)
    }

    override fun getMessageWithPaging(): LiveData<PagingData<MessageItemEntity>> {
        return localDataSource.getMessageWithPaging()
    }

    override fun insertMessage(conversationRes: ConversationRes): Long {
        return localDataSource.insertMessage(conversationRes)
    }

    override fun getLastMessageFromSystem(): MessageItemEntity? {
        return localDataSource.getLastMessageFromSystem()
    }

    override fun getLastMessageFrom(): MessageItemEntity? {
        return localDataSource.getLastMessageFrom()
    }

    override fun clearDatabase() {
        localDataSource.clearDatabase()
    }

    override fun getMessageById(id: Long): MessageItemEntity {
        return localDataSource.getMessageById(id)
    }

    override fun updateMessageItem(messageItem: MessageItemEntity) {
        localDataSource.updateMessageItem(messageItem)
    }

    override fun uploadFile(filePath: String): Single<UploadFileRes> {
        return restDataSource.uploadFile(filePath)
    }

    private fun getHistoryCall(
        getHistoryV1UseCase: GetHistoryV1UseCase,
        pageNumber: Int,
        pageSize: Int
    ): Single<List<MessageItemEntity>> {
        return getHistoryV1UseCase.execute(
            GetHistoryV1UseCase.Params(
                pageNumber = pageNumber,
                pageSize = pageSize
            )
        )
    }


    @OptIn(ExperimentalPagingApi::class)
    override fun getMessageDataSource(getHistoryV1UseCase: GetHistoryV1UseCase): LiveData<PagingData<MessageItemEntity>> {
        val size = 30
        val rm: GenericRemoteMediator<MessageItemEntity> by lazy {
            GenericRemoteMediator({
                appSettingDataSource.pageNumber
            }, {
                getHistoryCall(getHistoryV1UseCase, it, size)
            }, {
                Completable.fromAction {
                    appSettingDataSource.pageNumber = appSettingDataSource.pageNumber + 1
                    insertAllMessageItem(messageItemList = it)
                }
            })

        }

        val pagingConfig = PagingConfig(size, enablePlaceholders = false)
        val pager = Pager(
            pagingConfig,
            remoteMediator = rm,
            pagingSourceFactory = { getMessagePagingSource() })

        return pager.liveData
    }

    override fun getMessagePagingSource(): PagingSource<Int, MessageItemEntity> {
        return localDataSource.getMessagePagingSource()
    }


    override fun conversationFeedback(
        engagementId: String,
        usefulnessRating: String,
        accuracyRating: String,
        feedback: String
    ): Single<JsonElement> {
        return restDataSource.conversationFeedback(
            engagementId,
            usefulnessRating,
            accuracyRating,
            feedback
        )
    }

    override fun conversationFeedbackResponse(
        engagementId: String,
        responseId: String,
        responseRating: String,
    ): Single<JsonElement> {
        return restDataSource.conversationFeedbackResponse(
            engagementId,
            responseId,
            responseRating
        )
    }
}