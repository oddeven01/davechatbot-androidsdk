package com.sociograph.davechatbot.data.repositaries.remote

import androidx.annotation.VisibleForTesting
import com.google.gson.JsonElement
import com.sociograph.davechatbot.data.repositaries.BaseRepository
import com.sociograph.davechatbot.data.support_data.webservices.MultipartUtil
import com.sociograph.davechatbot.data.webservices.IService
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.datasources.remote.IRestDataSource
import com.sociograph.davechatbot.domain.resmodels.*
import io.reactivex.Single

internal class RestDataRepository(
    private val service: IService,
    private val appSettingDataSource: IAppSettingsDataSource
) : BaseRepository(), IRestDataSource {


    companion object {

        private var INSTANCE: RestDataRepository? = null

        @JvmStatic
        fun getInstance(
            service: IService,
            appSettingDataSource: IAppSettingsDataSource
        ): RestDataRepository {
            if (INSTANCE == null) {
                synchronized(RestDataRepository::javaClass) {
                    INSTANCE =
                        RestDataRepository(
                            service,
                            appSettingDataSource
                        )
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }

    override fun login(
        userName: String,
        password: String,
        roles: String,
        attrs: List<String>,
        enterPriseId: String
    ): Single<LoginRes> {
        val params = hashMapOf<String, Any>()
        params["user_id"] = userName
        params["password"] = password
        params["roles"] = roles
        params["attrs"] = attrs
        params["enterprise_id"] = enterPriseId
        return service.login(params)
    }

    override fun conversation(
        conversationId: String,
        daveUserId: String,
        customerResponse: String?,
        customerState: String?,
        engagementId: String?,
        systemResponse: String?,
        queryType: String?
    ): Single<ConversationRes> {
        val params = hashMapOf<String, Any?>()
        params["engagement_id"] = engagementId
        params["system_response"] = systemResponse
        params["customer_state"] = customerState
        params["customer_response"] = customerResponse
        params["query_type"] = queryType
        return service.conversation(conversationId, daveUserId, params, getHeaderParam())
    }

    private fun getHeaderParam(): Map<String, String> {
        val params = hashMapOf<String, String>()
        params["X-I2CE-ENTERPRISE-ID"] = appSettingDataSource.loginDetail?.enterpriseId ?: ""
        params["X-I2CE-USER-ID"] = appSettingDataSource.loginDetail?.userId ?: ""
        params["X-I2CE-API-KEY"] = appSettingDataSource.loginDetail?.apiKey ?: ""
        return params
    }

    override fun getConversationHistory(
        conversationId: String,
    ): Single<ConversationHistoryRes> {
        return service.conversationHistory(
            conversationId,
            appSettingDataSource.userId,
            getHeaderParam()
        )
    }

    override fun getConversationHistory(
        conversationId: String,
        pageSize: Int,
        pageNumber: Int
    ): Single<ConversationHistoryRes> {
        return service.conversationHistory(
            conversationId,
            appSettingDataSource.userId,
            pageSize, pageNumber,
            getHeaderParam()
        )
    }

    override fun getConversationKeywords(
        conversationId: String,
    ): Single<ConversationKeywordRes> {
        return service.conversationKeywords(
            conversationId,
            getHeaderParam()
        )
    }

    override fun uploadFile(filePath: String): Single<UploadFileRes> {
        val prepareFilePart = MultipartUtil.prepareFilePart("file", filePath)
        return service.uploadFile(true, prepareFilePart, getHeaderParam())
    }


    override fun conversationFeedback(
        engagementId: String,
        usefulnessRating: String,
        accuracyRating: String,
        feedback: String,
    ): Single<JsonElement> {
        val params = hashMapOf<String, Any?>()
        params["usefulness_rating"] = usefulnessRating
        params["accuracy_rating"] = accuracyRating
        params["feedback"] = feedback
        return service.conversationFeedback(engagementId, params, getHeaderParam())
    }

    override fun conversationFeedbackResponse(
        engagementId: String,
        responseId: String,
        responseRating: String,
    ): Single<JsonElement> {
        val params = hashMapOf<String, Any?>()
        params["response_rating"] = responseRating
        return service.conversationFeedback(engagementId, responseId, params, getHeaderParam())
    }

}
