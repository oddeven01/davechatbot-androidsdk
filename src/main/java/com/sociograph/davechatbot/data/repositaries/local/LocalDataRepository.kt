package com.sociograph.davechatbot.data.repositaries.local

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.paging.*
import com.google.gson.Gson
import com.sociograph.davechatbot.data.database.RoomDataSource
import com.sociograph.davechatbot.data.repositaries.BaseRepository
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.datasources.local.ILocalDataSource
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.mapper.ConversationResponseMapper
import com.sociograph.davechatbot.domain.mapper.UserResponseMapper
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.UserRes

internal class LocalDataRepository(
    private val appSettingDataSource: IAppSettingsDataSource,
    private val roomManager: RoomDataSource
) : BaseRepository(), ILocalDataSource {

    companion object {

        private var INSTANCE: LocalDataRepository? = null

        @JvmStatic
        fun getInstance(
            appSettingDataSource: IAppSettingsDataSource,
            roomManager: RoomDataSource
        ): LocalDataRepository {
            if (INSTANCE == null) {
                synchronized(LocalDataRepository::javaClass) {
                    INSTANCE =
                        LocalDataRepository(
                            appSettingDataSource,
                            roomManager
                        )
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }

    private val messageItemDao = roomManager.messageItemDao

    override fun insertMessage(userRes: UserRes): Long {
        return messageItemDao.insert(UserResponseMapper().map(userRes))
    }

    override fun insertMessage(conversationRes: ConversationRes): Long {
        return messageItemDao.insert(ConversationResponseMapper().map(conversationRes))
    }

    override fun updateResponseById(id: Long, userRes: UserRes) {
        val response = UserResponseMapper().map(userRes)
        return messageItemDao.updateResponseById(id, Gson().toJson(response))
    }

    override fun getMessageCount(): Int {
        return messageItemDao.getRowCount()
    }

    override fun getSystemMessageCount(): Int {
        return messageItemDao.getSystemMessageCount()
    }

    override fun insertAllMessageItem(messageItemList: List<MessageItemEntity>) {
        messageItemDao.insertAll(messageItemList)
    }

    override fun getMessageWithPaging(): LiveData<PagingData<MessageItemEntity>> {
        val pagingConfig = PagingConfig(25)
        val pager = Pager(
            config = pagingConfig,
            pagingSourceFactory = { messageItemDao.getMessages }
        )
        return pager.liveData
    }

    override fun getMessagePagingSource(): PagingSource<Int, MessageItemEntity> {
        return messageItemDao.getMessages
    }

    override fun getLastMessageFromSystem(): MessageItemEntity? {
        return messageItemDao.getLastMessageFromSystem
    }

    override fun getLastMessageFrom(): MessageItemEntity? {
        return messageItemDao.getLastMessageFrom
    }

    override fun clearDatabase() {
        messageItemDao.deleteMessages()
    }

    override fun getMessageById(id: Long): MessageItemEntity {
        return messageItemDao.getMessageById(id)
    }

    override fun updateMessageItem(messageItem: MessageItemEntity) {
        messageItemDao.update(messageItem)
    }
}