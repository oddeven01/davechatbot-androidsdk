package com.sociograph.davechatbot.data.ds


import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.sociograph.davechatbot.data.webservices.GsonUtils
import com.sociograph.davechatbot.data.webservices.JsonUtils
import com.sociograph.davechatbot.domain.resmodels.ConversationHistoryRes
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.utils.Constants
import java.lang.reflect.Type

internal class ConversationHistoryResDs : JsonDeserializer<ConversationHistoryRes> {

    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): ConversationHistoryRes {
        val gson = GsonUtils.getInstance().gson

        val jsonObject = json.asJsonObject

        val conversationHistoryRes = ConversationHistoryRes()

        if (JsonUtils.hasProperty(jsonObject, "history")) {
            val historyArray = jsonObject.get("history").asJsonArray

            historyArray.forEach {
                val jsonObjectItem = it.asJsonObject
                if (jsonObjectItem.get("direction").asString == Constants.DIRECTION_SYSTEM) {
                    val systemMessageItem = gson.fromJson(jsonObjectItem, ConversationRes::class.java)
                    conversationHistoryRes.history.add(systemMessageItem)
                } else {
                    val userMessageItem = gson.fromJson(jsonObjectItem, UserRes::class.java)
                    conversationHistoryRes.history.add(userMessageItem)
                }
            }
        }
        return conversationHistoryRes
    }
}
