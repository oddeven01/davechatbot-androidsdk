package com.sociograph.davechatbot.data.database

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Room

class RoomManager {

    companion object {

        private var INSTANCE: RoomDataSource? = null

        val DB_NAME = "DaveChatBoat-DB"

        @JvmStatic
        fun getInstance(applicationContext: Context): RoomDataSource {
            if (INSTANCE == null) {
                synchronized(RoomManager::javaClass) {
                    INSTANCE = create(applicationContext)
                }
            }
            return INSTANCE!!
        }

        private fun create(context: Context): RoomDataSource {
            val dbBuilder = Room.databaseBuilder(
                context,
                RoomDataSource::class.java,
                DB_NAME
            ).allowMainThreadQueries()

            if (context.getDatabasePath(DB_NAME).exists()) {
                dbBuilder.createFromFile(context.getDatabasePath(DB_NAME))
            }
            return dbBuilder.build()
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }

        fun destroyInstance() {
            if (INSTANCE?.isOpen == true) {
                INSTANCE?.close()
            }
            INSTANCE = null
        }
    }
}