package com.sociograph.davechatbot.data.database.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.support_domain.room.BaseDao
import io.reactivex.Completable


@Dao
abstract class MessageItemDao : BaseDao<MessageItemEntity>() {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAllMessageItem(obj: List<MessageItemEntity>): Completable

    @Query("UPDATE tbl_message_item SET response=:response where id=:id")
    abstract fun updateResponseById(id: Long, response: String)

    @Query("SELECT * FROM tbl_message_item where id=:id")
    abstract fun getMessageById(id: Long): MessageItemEntity

    @Query("SELECT COUNT(*) FROM tbl_message_item")
    abstract fun getRowCount(): Int

    @Query("SELECT COUNT(*) FROM tbl_message_item where is_from_me = 0")
    abstract fun getSystemMessageCount(): Int

    @get:Query("SELECT * FROM tbl_message_item where is_from_me = 0 ORDER BY sequence DESC LIMIT 1")
    abstract val getLastMessageFromSystem: MessageItemEntity?

    @get:Query("SELECT * FROM tbl_message_item ORDER BY sequence DESC LIMIT 1")
    abstract val getLastMessageFrom: MessageItemEntity?

    @get:Query("SELECT * FROM tbl_message_item ORDER BY sequence DESC, is_from_me")
    abstract val getMessages: PagingSource<Int, MessageItemEntity>

    @Query("DELETE FROM tbl_message_item")
    abstract fun deleteMessages()
}
