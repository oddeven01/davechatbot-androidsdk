package com.sociograph.davechatbot.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sociograph.davechatbot.data.database.dao.MessageItemDao
import com.sociograph.davechatbot.domain.entity.MessageItemEntity

@Database(
    entities = [MessageItemEntity::class],
    version = 1,
    exportSchema = false
)
abstract class RoomDataSource : RoomDatabase() {

    abstract val messageItemDao: MessageItemDao


}
