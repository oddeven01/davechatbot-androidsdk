package com.sociograph.davechatbot.utils

object Constants {
    const val STR_NAME_MAP = "_name_map"
    const val STR_TYPE_MAP = "_type_map"
    const val LOCAL_KEY = "local_path"
    const val DIRECTION_USER = "user"
    const val DIRECTION_SYSTEM = "system"
}