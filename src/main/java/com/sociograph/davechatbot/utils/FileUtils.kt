package com.sociograph.davechatbot.utils

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.webkit.MimeTypeMap
import androidx.core.net.toFile
import com.sociograph.davechatbot.core.Injection
import io.reactivex.Single
import okio.Okio
import java.io.File
import java.util.*

object FileUtils {

    private const val TAG = "FileUtils"
    private const val TEMP = "temp"

    fun copyFileToTemp(fileUri: Uri, fileName: String): Single<File> {
        val contentResolver = Injection.provideContext().contentResolver
        return Single.create { emitter ->
            try {
                val name = if (fileName.isEmpty()) {
                    String.format(
                        "DaveChat_%s.%s",
                        UUID.randomUUID().toString(),
                        generateExt(fileUri)
                    )
                } else {
                    fileName
                }
                val outputFile = File(getTempDir(), name)
                val bufferedSink = Okio.buffer(Okio.sink(outputFile))
                val openInputStream = contentResolver.openInputStream(fileUri)
                bufferedSink.write(openInputStream!!.readBytes())
                bufferedSink.close()
                emitter.onSuccess(outputFile)
            } catch (e: Exception) {
                e.printStackTrace()
                emitter.onError(e)
            }
        }
    }


    private fun getTempDir(): File {
        val dir = Injection.provideContext().getDir(TEMP, Context.MODE_PRIVATE)
        if (!dir.exists()) {
            dir.mkdir()
        }
        return dir
    }

    private fun generateExt(fileUri: Uri): String? {
        return getExtension(Injection.provideContext(), fileUri)
    }


    fun getMimeType(url: String): String? {
        var type: String? = null
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
        }
        return type
    }

    private fun getMimeType(context: Context, uri: Uri): String? {
        var mimeType: String?
        mimeType =
            if (uri.scheme != null && uri.scheme!! == ContentResolver.SCHEME_CONTENT) { //If scheme is a content
                context.contentResolver.getType(uri)
            } else { //If scheme is a File
                val extension =
                    MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(uri.toFile()).toString())
                MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
            }

        if (mimeType.isNullOrEmpty()) {
            mimeType = uri.getQueryParameter("mimeType")
        }
        return mimeType
    }

    fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children!!.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
        }
        // The directory is now empty so delete it
        return dir!!.delete()
    }

    private fun getExtension(context: Context, uri: Uri): String? {
        val mimeType = getMimeType(context, uri)
        val mime = MimeTypeMap.getSingleton()
        return mime.getExtensionFromMimeType(mimeType)
    }

    private fun getExtensionFromMimeType(mimeType: String): String? {
        return MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType)
    }

    val Double.sizeInKb get() = this / 1024
    val Double.sizeInMb get() = sizeInKb / 1024
    val Double.sizeInGb get() = sizeInMb / 1024
    val Double.sizeInTb get() = sizeInGb / 1024

    val File.size get() = if (!exists()) 0.0 else length().toDouble()
    val File.sizeInKb get() = size / 1024
    val File.sizeInMb get() = sizeInKb / 1024
    val File.sizeInGb get() = sizeInMb / 1024
    val File.sizeInTb get() = sizeInGb / 1024

    fun File.sizeStr(): String = size.toString()
    fun File.sizeStrInKb(decimals: Int = 0): String = "%.${decimals}f".format(sizeInKb)
    fun File.sizeStrInMb(decimals: Int = 0): String = "%.${decimals}f".format(sizeInMb)
    fun File.sizeStrInGb(decimals: Int = 0): String = "%.${decimals}f".format(sizeInGb)

    fun File.sizeStrWithBytes(): String = sizeStr() + "b"
    fun File.sizeStrWithKb(decimals: Int = 0): String = sizeStrInKb(decimals) + "Kb"
    fun File.sizeStrWithMb(decimals: Int = 0): String = sizeStrInMb(decimals) + "Mb"
    fun File.sizeStrWithGb(decimals: Int = 0): String = sizeStrInGb(decimals) + "Gb"

}