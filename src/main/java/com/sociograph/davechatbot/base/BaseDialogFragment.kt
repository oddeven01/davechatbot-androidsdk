package com.sociograph.davechatbot.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.databinding.FBaseDialogBinding
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


fun AppCompatDialogFragment.showDialog(
    targetFragment: Fragment,
    requestKey: String,
    listener: ((requestKey: String, bundle: Bundle) -> Unit)
) {
    if (targetFragment.activity != null) {
        if (!targetFragment.requireActivity().supportFragmentManager.isStateSaved) {
            this.show(targetFragment.parentFragmentManager, this.javaClass.canonicalName)
            this.setFragmentResultListener(requestKey, listener)
        }
    }
}

fun AppCompatDialogFragment.showDialog(fragmentManager: FragmentManager) {
    if (!fragmentManager.isStateSaved) {
        this.show(fragmentManager, this.javaClass.canonicalName)
    }
}

internal abstract class BaseDialogFragment<VM : BaseViewModel> : AppCompatDialogFragment() {

    private val compositeDisposable = CompositeDisposable()


    protected lateinit var viewModel: VM
    protected abstract fun initializeViewModel(): VM

    private var fBaseBinding: FBaseDialogBinding? = null

    abstract fun setUpUI()

    protected abstract fun getLayoutView(
        inflater: LayoutInflater
    ): View?

    protected open fun getProgressView(): View? {
        return fBaseBinding?.fBaseLoadProgress
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        fBaseBinding = FBaseDialogBinding.inflate(inflater, container, false)

        val layoutView = getLayoutView(inflater)

        fBaseBinding?.fBaseContent?.addView(layoutView)

        viewModel = initializeViewModel()

        readArguments()

        setUpObserver()

        return fBaseBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpUI()
    }

    fun addRxCall(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    private fun clearAllCalls() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }
    }

    private fun setUpObserver() {

        viewModel.showDataProgress.observe(viewLifecycleOwner, Observer { show ->
            if (show) {
                showDataProgress()
            } else {
                hideDataProgress()
            }
        })
        viewModel.showProgress.observe(viewLifecycleOwner, { show ->
            if (show) {
                showProgress()
            } else {
                hideProgress()
            }
        })

    }

    public fun showDataProgress() {
        getProgressView()?.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.white
            )
        )
        getProgressView()?.visibility = View.VISIBLE
    }

    public fun hideDataProgress() {
        getProgressView()?.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                android.R.color.transparent
            )
        )
        getProgressView()?.visibility = View.GONE
    }

    public fun showProgress() {
        getProgressView()?.visibility = View.VISIBLE

    }

    public fun hideProgress() {
        getProgressView()?.visibility = View.GONE

    }


    override fun onDestroy() {
        clearAllCalls()
        super.onDestroy()
    }

    fun showToast(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }

    fun showToast(@StringRes resId: Int) {
        Toast.makeText(requireContext(), resId, Toast.LENGTH_SHORT).show()
    }

    fun isInternetAvailable(): Boolean {
        return true
    }

    open fun readArguments() {}

}
