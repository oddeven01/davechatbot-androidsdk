package com.sociograph.davechatbot.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel constructor() : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    var showProgress: MutableLiveData<Boolean> = MutableLiveData()

    var showDataProgress: MutableLiveData<Boolean> = MutableLiveData()

    var showProgressDialog: MutableLiveData<Boolean> = MutableLiveData()

    var showSoftMessage: MutableLiveData<String> = MutableLiveData()


    fun showProgressDialog() {
        showProgressDialog.value = true
    }

    fun hideProgressDialog() {
        showProgressDialog.value = false
    }

    abstract fun subscribe()
    open fun unsubscribe() {
        clearAllCalls()
    }

    fun addRxCall(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    private fun clearAllCalls() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.clear()
        }
    }
}
