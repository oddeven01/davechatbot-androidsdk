package com.sociograph.davechatbot.ui.chat.view_holder

import android.content.res.ColorStateList
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.domain.models.LoadingMessageItem

internal class LoadingMessageViewHolder(
    itemView: View,
    private val uiSetting: UISetting
) : BaseViewHolder<LoadingMessageItem>(itemView) {

    companion object {
        var OUTGOING_LAYOUT_ID = R.layout.r_outgoing_loading_view
        var INCOMING_LAYOUT_ID = R.layout.r_incoming_loading_view
    }

    override fun bindViewHolder(item: LoadingMessageItem) {
        val llBubble = itemView.findViewById<View>(R.id.llBubble)
        llBubble.backgroundTintList = ColorStateList.valueOf(uiSetting.chatBubbleColor)

        itemView.findViewById<AppCompatImageView>(R.id.imgChatBot)
            ?.setImageDrawable(uiSetting.chatBotIcon)

        itemView.findViewById<AppCompatImageView>(R.id.imgChatUser)
            ?.setImageDrawable(uiSetting.chatUserIcon)

        itemView.findViewById<ProgressBar>(R.id.progress)?.indeterminateTintList =
            ColorStateList.valueOf(uiSetting.headerColor)


    }
}