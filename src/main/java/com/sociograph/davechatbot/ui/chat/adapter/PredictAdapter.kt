package com.sociograph.davechatbot.ui.chat.adapter

import android.view.View
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.base.BaseRecyclerViewAdapter
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.RPredictItemViewBinding
import com.sociograph.davechatbot.domain.models.KeywordItem


class PredictAdapter :
    BaseRecyclerViewAdapter<KeywordItem, PredictAdapter.PredictViewHolder>() {


    override fun getRowLayoutId(viewType: Int): Int {
        return R.layout.r_predict_item_view
    }

    override fun getViewHolder(view: View, viewType: Int): PredictViewHolder {
        return PredictViewHolder(view)
    }

    override fun bind(
        viewHolder: PredictViewHolder,
        position: Int,
        item: KeywordItem
    ) {
        viewHolder.bindViewHolder(item)
    }


    inner class PredictViewHolder(itemView: View) :
        BaseViewHolder<KeywordItem>(itemView) {

        private val bind = RPredictItemViewBinding.bind(itemView)

        override fun bindViewHolder(item: KeywordItem) {
            bind.txtOptions.text = item.customerResponse
            bind.txtOptions.setOnClickListener {
                clickListener?.onItemClick(adapterPosition, item)
            }
        }
    }

}