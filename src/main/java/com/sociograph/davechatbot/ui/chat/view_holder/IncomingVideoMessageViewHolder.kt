package com.sociograph.davechatbot.ui.chat.view_holder

import android.content.res.ColorStateList
import android.view.View
import androidx.core.view.isVisible
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.RIncomingVideoViewBinding
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.support_domain.ext.getMessageTime
import com.sociograph.davechatbot.extension.fromNormalHtml
import com.sociograph.davechatbot.interfaces.MessageAdapterListener

internal class IncomingVideoMessageViewHolder(
    itemView: View,
    private val messageAdapterListener: MessageAdapterListener?,
    private val uiSetting: UISetting
) :
    BaseViewHolder<MessageItemEntity>(itemView) {

    companion object {
        var LAYOUT_ID = R.layout.r_incoming_video_view
    }

    private val bind = RIncomingVideoViewBinding.bind(itemView)



    override fun bindViewHolder(item: MessageItemEntity) {
        val conversationItem = item.response as ConversationRes

        bind.llBubble.backgroundTintList = ColorStateList.valueOf(uiSetting.chatBubbleColor)
        bind.incPlainTextView.txtPlainMessage.setTextColor(uiSetting.chatBubbleTextColor)

        bind.imgChatBot.setImageDrawable(uiSetting.chatBotIcon)
        bind.incPlainTextView.txtPlainMessage.text = conversationItem.placeholder?.fromNormalHtml()
        bind.incPlainTextView.txtTime.text = item.createAt.getMessageTime()

        bind.incPlainTextView.txtName.text = uiSetting.titleText

        bind.videoView.setVideoPath(conversationItem.data?.video)
        /*val mediaController = MediaController(itemView.context)*/
        bind.videoView.requestFocus()
        bind.videoView.setOnPreparedListener {
            if (bind.videoView.isPlaying.not()) {
                bind.videoView.start()
                /*mediaController.show(3000)*/
            }
        }

        bind.llFeedback.isVisible = conversationItem.showFeedback

        bind.imgThumbDown.setOnClickListener {
            messageAdapterListener?.onSubmitFeedback(conversationItem.responseId ?: "", "1")
        }
        bind.imgThumbUp.setOnClickListener {
            messageAdapterListener?.onSubmitFeedback(conversationItem.responseId ?: "", "5")
        }
        /*mediaController.setAnchorView(bind.videoView)
        bind.videoView.setMediaController(mediaController)*/

        /*bind.videoView.setVideoPath("http://techslides.com/demos/sample-videos/small.mp4")
        val mediaController = MediaController(itemView.context)
        mediaController.requestFocus()
        bind.videoView.setOnPreparedListener(MediaPlayer.OnPreparedListener {
            mediaController.show(
                0
            )
        })
        mediaController.setAnchorView(bind.videoView)
        bind.videoView.setMediaController(mediaController)
        bind.videoView.start()*/
    }

    /*override fun onViewDetachedFromWindow() {
        super.onViewDetachedFromWindow()
        if (bind.videoView.isPlaying) {
            bind.videoView.stopPlayback();
        }
    }*/
}