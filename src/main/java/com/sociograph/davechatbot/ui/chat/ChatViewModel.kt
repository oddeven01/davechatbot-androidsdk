package com.sociograph.davechatbot.ui.chat

import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewModel
import com.sociograph.davechatbot.core.ConfigData
import com.sociograph.davechatbot.core.Injection
import com.sociograph.davechatbot.data.webservices.hasProperty
import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.models.KeywordItem
import com.sociograph.davechatbot.domain.models.UI_ELEMENT
import com.sociograph.davechatbot.domain.resmodels.FormItem
import com.sociograph.davechatbot.domain.resmodels.StateOptions
import com.sociograph.davechatbot.domain.support_domain.ext.DateFormatType
import com.sociograph.davechatbot.domain.support_domain.ext.parseDate
import com.sociograph.davechatbot.domain.support_domain.schedulers.Scheduler
import com.sociograph.davechatbot.domain.usecase.DisableFormMessageUseCase
import com.sociograph.davechatbot.domain.usecase.SendMessageUseCase
import com.sociograph.davechatbot.domain.usecase.UploadFileUseCase
import com.sociograph.davechatbot.extension.isJSONValid
import com.sociograph.davechatbot.utils.AppEvent
import com.sociograph.davechatbot.utils.Constants
import com.sociograph.davechatbot.utils.RxBus
import com.sociograph.davechatbot.utils.SingleLiveEvent
import io.reactivex.Single
import org.joda.time.DateTime
import java.io.File

internal class ChatViewModel(
    private val appSettingsDataSource: IAppSettingsDataSource,
    private val appDataSource: IAppDataSource,
) : BaseViewModel() {

    val stateOptionsListLive = SingleLiveEvent<List<StateOptions>>()
    val hideLoadingView = SingleLiveEvent<Boolean>()
    val showLoadingView = SingleLiveEvent<Boolean>()
    val historyProgressView = SingleLiveEvent<Boolean>()
    private var keywordItemList = listOf<KeywordItem>()
    var showPredictedListLive = SingleLiveEvent<List<KeywordItem>>()
    val submitFeedbackSuccess = SingleLiveEvent<Nothing>()

    private val sendMessageUseCase = Injection.provideSendMessageUseCase()
    private val getHistoryUseCase = Injection.provideGetHistoryUseCase()
    private val disableFormMessageUseCase = Injection.provideDisableFormMessageUseCase()

    private var getHistoryV1UseCase = Injection.provideGetHistoryV1UseCase()

    val dataSource: LiveData<PagingData<MessageItemEntity>> by lazy {
        appDataSource.getMessageDataSource(getHistoryV1UseCase).cachedIn(this)
    }

    override fun subscribe() {
        addRxCall(
            RxBus.listen(AppEvent.LoadingInitialHistory::class.java).subscribeOn(Scheduler.io())
                .observeOn(Scheduler.ui()).subscribe {
                    historyProgressView.postValue(it.isLoading)
                    if (!it.isLoading) {
                        if (appSettingsDataSource.isLoadedFirstTime.not()) {
                            sendMessage(null, null)
                            appSettingsDataSource.isLoadedFirstTime = true
                            resetCountDownTimer()
                        }
                    }
                })

        addRxCall(
            appDataSource.getConversationKeywords(ConfigData.CONVERSATION_ID)
                .subscribeOn(Scheduler.io())
                .observeOn(Scheduler.ui()).subscribe({
                    keywordItemList = it.keywords
                }, {

                })
        )


    }

    fun sendMessage(
        customerResponse: String?,
        customerState: String?,
        queryType: String? = "auto"
    ) {
        showLoadingView(true)
        val params = SendMessageUseCase.Params(
            ConfigData.CONVERSATION_ID,
            customerResponse,
            customerState,
            queryType
        )
        addRxCall(
            sendMessageUseCase.execute(params).subscribeOn(Scheduler.io())
                .observeOn(Scheduler.ui()).subscribe({
                    resetCountDownTimer()
                    hideLoadingView(true)
                    handleResponse(it)
                }, {
                    hideLoadingView(true)
                })
        )
    }

    private fun handleResponse(stateOptions: List<StateOptions>) {
        stateOptionsListLive.value = stateOptions
    }

    fun showLoadingView(isIncoming: Boolean) {
        showLoadingView.value = isIncoming
    }


    fun hideLoadingView(isIncoming: Boolean) {
        hideLoadingView.value = isIncoming
    }

    fun submitForm(id: Long, customerState: String?, item: List<FormItem>) {
        showLoadingView(true)
        addRxCall(
            uploadFileUseCase(item)
                .subscribeOn(Scheduler.io())
                .observeOn(Scheduler.ui())
                .subscribe({
                    sendMessage(
                        generateCustomerResponse(it).toString(),
                        customerState,
                        queryType = "click"
                    )
                    addRxCall(
                        disableFormMessageUseCase.execute(DisableFormMessageUseCase.Params(id))
                            .subscribeOn(Scheduler.io())
                            .observeOn(Scheduler.ui()).subscribe({}, {})
                    )
                }, {
                    hideLoadingView(true)
                    it.printStackTrace()
                    showSoftMessage.value = Injection.provideContext()
                        .getString(R.string.msg_something_went_wrong_try_again)
                })
        )

    }

    private fun generateCustomerResponse(item: List<FormItem>): JsonObject {
        val jsonObject = JsonObject()

        item.forEach { formItem ->

            if (formItem.ui_element == UI_ELEMENT.DATETIME.value && formItem.value.isNullOrEmpty()) {
                formItem.value = DateTime.now().parseDate(DateFormatType.dd_MM_yyyy_HH_mm)
            }

            if (formItem.ui_element == UI_ELEMENT.FILE.value) {
                jsonObject.addProperty(formItem.name, formItem.serverPath.trim())

                val fileName: String = formItem.value?.trim()?.let {
                    val file = File(it)
                    if (file.exists())
                        file.name else
                        ""
                } ?: ""

                if (jsonObject.hasProperty(Constants.LOCAL_KEY)) {
                    jsonObject.getAsJsonObject(Constants.LOCAL_KEY)
                        .addProperty(formItem.name, fileName)
                } else {
                    val nameMapObj = JsonObject()
                    nameMapObj.addProperty(formItem.name, fileName)
                    jsonObject.add(Constants.LOCAL_KEY, nameMapObj)
                }

            } else if (formItem.ui_element == UI_ELEMENT.SELECT.value) {
                jsonObject.addProperty(formItem.name, formItem.value?.trim())

                if (jsonObject.hasProperty(Constants.LOCAL_KEY)) {
                    jsonObject.getAsJsonObject(Constants.LOCAL_KEY)
                        .addProperty(formItem.name, formItem.displayValue)
                } else {
                    val nameMapObj = JsonObject()
                    nameMapObj.addProperty(formItem.name, formItem.displayValue)
                    jsonObject.add(Constants.LOCAL_KEY, nameMapObj)
                }
            } else if (formItem.ui_element == UI_ELEMENT.GEOLOCATION.value) {
                val value = formItem.value?.trim() ?: ""
                if (value.isJSONValid()) {
                    val locationObj = JsonParser.parseString(value).asJsonObject
                    jsonObject.addProperty("lat", locationObj.get("lat").asString)
                    jsonObject.addProperty("lng", locationObj.get("lng").asString)
                }
            } else {
                jsonObject.addProperty(formItem.name, formItem.value?.trim())
            }


            if (jsonObject.hasProperty(Constants.STR_NAME_MAP)) {
                jsonObject.getAsJsonObject(Constants.STR_NAME_MAP)
                    .addProperty(formItem.name, formItem.title)
            } else {
                val nameMapObj = JsonObject()
                nameMapObj.addProperty(formItem.name, formItem.title)
                jsonObject.add(Constants.STR_NAME_MAP, nameMapObj)
            }

            if (jsonObject.hasProperty(Constants.STR_TYPE_MAP)) {
                jsonObject.getAsJsonObject(Constants.STR_TYPE_MAP)
                    .addProperty(formItem.name, formItem.ui_element)
            } else {
                val typeMapObj = JsonObject()
                typeMapObj.addProperty(formItem.name, formItem.ui_element)
                jsonObject.add(Constants.STR_TYPE_MAP, typeMapObj)
            }
        }
        return jsonObject
    }

    fun clearEngagementId() {
        appSettingsDataSource.engagementId = null
        appSettingsDataSource.systemResponse = null
    }

    fun showPredictedList(keyword: String) {
        val filteredList = keywordItemList.filter { item ->
            item.keywordList.any { str ->
                str.split(" ")
                    .any { word ->
                        word.lowercase() == keyword.lowercase()
                    }
            } or item.customerResponse.split(" ").any { it.lowercase() == keyword.lowercase() }
        }
        if (filteredList.isNotEmpty()) {
            showPredictedListLive.value = filteredList
        }
    }


    private fun uploadFileUseCase(formItem: List<FormItem>): Single<List<FormItem>> {
        val fileListAvailableForUpload = formItem.filter {
            it.ui_element == UI_ELEMENT.FILE.value && it.value.isNullOrEmpty().not()
        }
        return if (fileListAvailableForUpload.isNotEmpty()) {
            Injection.provideUploadFileUseCase()
                .execute(UploadFileUseCase.Params(fileListAvailableForUpload))
        } else {
            Single.just(formItem)
        }
    }

    private var countDownTimer: CountDownTimer? = null
    fun resetCountDownTimer() {
        Log.w("CountDownTimer", "resetCountDownTimer")
        countDownTimer?.cancel()
        /* if (appSettingsDataSource.waitingTime != -1) {
             countDownTimer =
                 object : CountDownTimer(appSettingsDataSource.waitingTime.toLong(), 1000) {
                     override fun onTick(millisUntilFinished: Long) {

                     }

                     override fun onFinish() {
                         Log.w("CountDownTimer", "Execute Ping:- " + appSettingsDataSource.followUps)
                         sendMessage(null, appSettingsDataSource.followUps)
                     }
                 }
             Log.w("CountDownTimer", "start timer for " + appSettingsDataSource.waitingTime)
             countDownTimer?.start()
         }*/
    }

    fun cancelTimer() {
        countDownTimer?.cancel()
    }

    fun submitFeedback(responseId: String,response: String) {
        appSettingsDataSource.engagementId?.let {
            showProgressDialog()
            addRxCall(
                appDataSource.conversationFeedbackResponse(
                    it,
                    responseId,
                    response
                ).subscribeOn(Scheduler.io())
                    .observeOn(Scheduler.ui()).subscribe({
                        hideProgressDialog()
                        submitFeedbackSuccess.call()
                    }, {
                        hideProgressDialog()
                        showSoftMessage.value = it.message
                    })
            )
        }
    }

    val uiSetting: UISetting by lazy {
        appSettingsDataSource.uiSetting
    }
}
