package com.sociograph.davechatbot.ui.chat.adapter


import android.view.View
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.base.BaseRecyclerViewAdapter
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.ROptionViewBinding
import com.sociograph.davechatbot.domain.resmodels.Options

class OptionsAdapter :
    BaseRecyclerViewAdapter<Options, OptionsAdapter.OptionsViewHolder>() {


    override fun getRowLayoutId(viewType: Int): Int {
        return R.layout.r_option_view
    }

    override fun getViewHolder(view: View, viewType: Int): OptionsViewHolder {
        return OptionsViewHolder(view)
    }

    override fun bind(
        viewHolder: OptionsViewHolder,
        position: Int,
        item: Options
    ) {
        viewHolder.bindViewHolder(item)
    }


    inner class OptionsViewHolder(itemView: View) :
        BaseViewHolder<Options>(itemView) {

        private val bind = ROptionViewBinding.bind(itemView)

        override fun bindViewHolder(item: Options) {
            bind.txtOptions.text = item.customerResponse

            bind.root.setOnClickListener {
                clickListener?.onItemClick(adapterPosition, item)
            }
        }
    }

}