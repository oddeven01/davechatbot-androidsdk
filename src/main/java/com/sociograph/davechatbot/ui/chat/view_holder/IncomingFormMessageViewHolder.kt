package com.sociograph.davechatbot.ui.chat.view_holder

import android.content.res.ColorStateList
import android.view.View
import androidx.core.view.isVisible
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.RIncomingFormViewBinding
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.models.UI_ELEMENT
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.support_domain.ext.getMessageTime
import com.sociograph.davechatbot.extension.fromNormalHtml
import com.sociograph.davechatbot.extension.isValidEmail
import com.sociograph.davechatbot.interfaces.MessageAdapterListener
import com.sociograph.davechatbot.ui.chat.adapter.FormInputsAdapter

internal class IncomingFormMessageViewHolder(
    itemView: View,
    private val messageAdapterListener: MessageAdapterListener?,
    private val uiSetting: UISetting
) :
    BaseViewHolder<MessageItemEntity>(itemView) {

    companion object {
        var LAYOUT_ID = R.layout.r_incoming_form_view
    }

    private val bind = RIncomingFormViewBinding.bind(itemView)

    override fun bindViewHolder(item: MessageItemEntity) {
        var conversationItem = item.response as ConversationRes


        bind.llBubble.backgroundTintList = ColorStateList.valueOf(uiSetting.chatBubbleColor)
        bind.incPlainTextView.txtPlainMessage.setTextColor(uiSetting.chatBubbleTextColor)
        bind.imgChatBot.setImageDrawable(uiSetting.chatBotIcon)
        bind.btnSubmit.backgroundTintList = ColorStateList.valueOf(uiSetting.buttonColor)
        bind.btnSubmit.setTextColor(uiSetting.buttonTextColor)


        messageAdapterListener?.getFilledConversationData(conversationItem.responseId ?: "")?.let {
            conversationItem = it
        }
        val inputItems = conversationItem.data?.form?.toMutableList() ?: mutableListOf()

        bind.incPlainTextView.txtPlainMessage.text = conversationItem.placeholder?.fromNormalHtml()
        bind.incPlainTextView.txtTime.text = item.createAt.getMessageTime()
        bind.incPlainTextView.txtName.text = uiSetting.titleText
        val formInputsAdapter =
            FormInputsAdapter(conversationItem.enableForm, messageAdapterListener, conversationItem)
        bind.btnSubmit.isEnabled = conversationItem.enableForm
        formInputsAdapter.setItems(inputItems)
        bind.rvFormInputs.adapter = formInputsAdapter
        bind.btnSubmit.setOnClickListener {
            val indexOfFirstEmptyInput =
                inputItems.indexOfFirst {
                    it.required && it.value.isNullOrEmpty()
                }

            val indexOfInvalidMobileNumber = inputItems.indexOfFirst {
                it.name == "mobile_number" &&
                        ((it.value?.startsWith("+") == true && it.value?.length!! != 13) ||
                                it.value?.length != 10)
            }


            val indexOfInvalidEmail = inputItems.indexOfFirst {
                val value = it.value ?: ""
                it.name == "email" && !value.isValidEmail()
            }

            if (indexOfFirstEmptyInput != -1) {
                val formViewHolder = bind
                    .rvFormInputs
                    .findViewHolderForAdapterPosition(indexOfFirstEmptyInput)
                val formItem = inputItems[indexOfFirstEmptyInput]
                when (formItem.ui_element) {
                    UI_ELEMENT.DATE.value,
                    UI_ELEMENT.DATETIME.value -> {
                        (formViewHolder as FormInputsAdapter.FormDateInputsViewHolder)
                            .bind.edtInput.performClick()
                    }
                    UI_ELEMENT.FILE.value -> {
                        (formViewHolder as FormInputsAdapter.FormFileInputsViewHolder)
                            .bind.txtError.apply {
                                text = formItem.error
                                isVisible = true
                            }
                    }
                    UI_ELEMENT.SELECT.value -> {
                        (formViewHolder as FormInputsAdapter.FormSelectInputsViewHolder)
                            .bind.txtError.apply {
                                text = formItem.error
                                isVisible = true
                            }
                    }

                    UI_ELEMENT.GEOLOCATION.value -> {
                        (formViewHolder as FormInputsAdapter.FormGeoLocationViewHolder)
                            .bind.txtError.apply {
                                text = formItem.error
                                isVisible = true
                            }
                    }
                    else -> {
                        (formViewHolder as FormInputsAdapter.FormInputsViewHolder)
                            .bind.edtInput.error = formItem.error
                    }
                }

            } else {
                if (indexOfInvalidMobileNumber != -1) {
                    val formViewHolder = bind
                        .rvFormInputs
                        .findViewHolderForAdapterPosition(indexOfInvalidMobileNumber)
                    val formItem = inputItems[indexOfInvalidMobileNumber]
                    (formViewHolder as FormInputsAdapter.FormInputsViewHolder)
                        .bind.edtInput.error = formItem.error
                } else {
                    if (indexOfInvalidEmail != -1) {
                        val formViewHolder = bind
                            .rvFormInputs
                            .findViewHolderForAdapterPosition(indexOfInvalidEmail)
                        val formItem = inputItems[indexOfInvalidEmail]
                        (formViewHolder as FormInputsAdapter.FormInputsViewHolder)
                            .bind.edtInput.error = formItem.error
                    } else {
                        formInputsAdapter.updateFormState(false)
                        submitForm(item.id, conversationItem)
                    }
                }
            }
        }
    }

    private fun submitForm(
        id: Long, conversationItem: ConversationRes
    ) {
        conversationItem.enableForm = false
        bind.btnSubmit.isEnabled = conversationItem.enableForm

        val formList =
            messageAdapterListener?.getFilledConversationData(conversationItem.responseId ?: "")
                ?.let {
                    it.data?.form
                } ?: run {
                conversationItem.data?.form
            }

        messageAdapterListener?.onSubmitForm(
            id,
            conversationItem.data?.customerState,
            formList ?: listOf()
        )
    }
}