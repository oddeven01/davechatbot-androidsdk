package com.sociograph.davechatbot.ui.chat.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.models.LoadingMessageItem
import com.sociograph.davechatbot.domain.models.ResponseType
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.interfaces.MessageAdapterListener
import com.sociograph.davechatbot.ui.chat.view_holder.*

internal class MessagePagedAdapter(private val uiSetting: UISetting) :
    PagingDataAdapter<MessageItemEntity, BaseViewHolder<MessageItemEntity>>(MessageItemEntity.DIFF_CALLBACK) {

    var messageAdapterListener: MessageAdapterListener? = null

    override fun onBindViewHolder(holder: BaseViewHolder<MessageItemEntity>, position: Int) {
        getItem(position)?.let { messageItem ->
            holder.bindViewHolder(messageItem)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<MessageItemEntity> {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return getViewHolder(view, viewType)
    }

    private fun getViewHolder(view: View, viewType: Int): BaseViewHolder<MessageItemEntity> {
        when (viewType) {
            OutgoingMessageViewHolder.LAYOUT_ID -> {
                return OutgoingMessageViewHolder(view, messageAdapterListener, uiSetting)
            }

            IncomingMessageViewHolder.LAYOUT_ID -> {
                return IncomingMessageViewHolder(view, messageAdapterListener, uiSetting)
            }

            /*LoadingMessageViewHolder.INCOMING_LAYOUT_ID, LoadingMessageViewHolder.OUTGOING_LAYOUT_ID -> {
                return LoadingMessageViewHolder(view, messageAdapterListener)
            }*/

            IncomingOptionsMessageViewHolder.LAYOUT_ID -> {
                return IncomingOptionsMessageViewHolder(view, messageAdapterListener, uiSetting)
            }

            IncomingImageMessageViewHolder.LAYOUT_ID -> {
                return IncomingImageMessageViewHolder(view, messageAdapterListener, uiSetting)
            }
            IncomingURLMessageViewHolder.LAYOUT_ID -> {
                return IncomingURLMessageViewHolder(view, messageAdapterListener, uiSetting)
            }
            IncomingVideoMessageViewHolder.LAYOUT_ID -> {
                return IncomingVideoMessageViewHolder(view, messageAdapterListener, uiSetting)
            }
            IncomingFormMessageViewHolder.LAYOUT_ID -> {
                return IncomingFormMessageViewHolder(view, messageAdapterListener, uiSetting)
            }
            IncomingThumbnailsMessageViewHolder.LAYOUT_ID -> {
                return IncomingThumbnailsMessageViewHolder(view, messageAdapterListener, uiSetting)
            }

        }
        return OutgoingMessageViewHolder(view, messageAdapterListener, uiSetting)
    }

    override fun getItemViewType(position: Int): Int {
        when (val messageItem = getItem(position)?.response) {
            is UserRes -> {
                return OutgoingMessageViewHolder.LAYOUT_ID
            }
            is LoadingMessageItem -> {
                return if (messageItem.isIncoming) {
                    LoadingMessageViewHolder.INCOMING_LAYOUT_ID
                } else {
                    LoadingMessageViewHolder.OUTGOING_LAYOUT_ID
                }
            }

            is ConversationRes -> {
                return getIncomingLayoutId(messageItem)
            }
        }

        return super.getItemViewType(position)
    }

    private fun getIncomingLayoutId(conversationItem: ConversationRes): Int {
        return when (conversationItem.data?.responseType) {
            ResponseType.OPTIONS.value -> {
                IncomingOptionsMessageViewHolder.LAYOUT_ID
            }
            ResponseType.FORM.value -> {
                IncomingFormMessageViewHolder.LAYOUT_ID
            }
            ResponseType.IMAGE.value -> {
                IncomingImageMessageViewHolder.LAYOUT_ID
            }
            ResponseType.URL.value -> {
                IncomingURLMessageViewHolder.LAYOUT_ID
            }
            ResponseType.VIDEO.value -> {
                IncomingVideoMessageViewHolder.LAYOUT_ID
            }
            ResponseType.THUMBNAILS.value -> {
                IncomingThumbnailsMessageViewHolder.LAYOUT_ID
            }
            else -> {
                IncomingMessageViewHolder.LAYOUT_ID
            }
        }
    }

}