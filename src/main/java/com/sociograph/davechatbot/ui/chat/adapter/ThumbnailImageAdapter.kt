package com.sociograph.davechatbot.ui.chat.adapter


import android.view.View
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.base.BaseRecyclerViewAdapter
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.RThumbnailImageViewBinding
import com.sociograph.davechatbot.domain.resmodels.ThumbImageItem
import com.sociograph.davechatbot.extension.loadImage

class ThumbnailImageAdapter :
    BaseRecyclerViewAdapter<ThumbImageItem, ThumbnailImageAdapter.OptionsViewHolder>() {


    override fun getRowLayoutId(viewType: Int): Int {
        return R.layout.r_thumbnail_image_view
    }

    override fun getViewHolder(view: View, viewType: Int): OptionsViewHolder {
        return OptionsViewHolder(view)
    }

    override fun bind(
        viewHolder: OptionsViewHolder,
        position: Int,
        item: ThumbImageItem
    ) {
        viewHolder.bindViewHolder(item)
    }


    inner class OptionsViewHolder(itemView: View) :
        BaseViewHolder<ThumbImageItem>(itemView) {

        private val bind = RThumbnailImageViewBinding.bind(itemView)

        override fun bindViewHolder(item: ThumbImageItem) {
            bind.imgView.loadImage(item.image)
            bind.txtTitle.text = item.title
            bind.root.setOnClickListener {
                clickListener?.onItemClick(absoluteAdapterPosition, item)
            }
        }
    }

}