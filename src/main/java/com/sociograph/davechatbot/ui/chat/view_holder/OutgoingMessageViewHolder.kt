package com.sociograph.davechatbot.ui.chat.view_holder

import android.content.res.ColorStateList
import android.view.View
import com.google.gson.JsonParser
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.ROutgoingPlainTextViewBinding
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.models.UI_ELEMENT
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.domain.support_domain.ext.DateFormatType
import com.sociograph.davechatbot.domain.support_domain.ext.getMessageTime
import com.sociograph.davechatbot.domain.support_domain.ext.parseDate
import com.sociograph.davechatbot.extension.fromNormalHtml
import com.sociograph.davechatbot.extension.isJSONValid
import com.sociograph.davechatbot.interfaces.MessageAdapterListener
import com.sociograph.davechatbot.utils.Constants

internal class OutgoingMessageViewHolder(
    itemView: View,
    messageAdapterListener: MessageAdapterListener?,
    private val uiSetting: UISetting
) :
    BaseViewHolder<MessageItemEntity>(itemView) {

    companion object {
        var LAYOUT_ID = R.layout.r_outgoing_plain_text_view
    }

    private val bind = ROutgoingPlainTextViewBinding.bind(itemView)

    override fun bindViewHolder(item: MessageItemEntity) {
        val userMessageItemEntity = item.response as UserRes

        bind.llBubble.backgroundTintList = ColorStateList.valueOf(uiSetting.chatBubbleColor)
        bind.incPlainTextView.txtPlainMessage.setTextColor(uiSetting.chatBubbleTextColor)
        bind.imgChatUser.setImageDrawable(uiSetting.chatUserIcon)

        bind.incPlainTextView.txtPlainMessage.text =
            getDisplayResponse(userMessageItemEntity.customerResponse).fromNormalHtml()
        bind.incPlainTextView.txtTime.text = item.createAt.getMessageTime()
        bind.incPlainTextView.txtName.text = "You"
    }

    private fun getDisplayResponse(response: String?): String {
        if (response != null && response.isJSONValid()) {
            val displayMessage = StringBuffer()
            val jsonObject = JsonParser.parseString(response).asJsonObject

            jsonObject.entrySet().forEach {
                if (it.value.isJsonPrimitive) {
                    var itemValue = it.value.asString

                    when (it.key) {
                        "lat" -> {
                            displayMessage.append("<b>")
                            displayMessage.append("Lat")
                            displayMessage.append(":")
                            displayMessage.append("</b> ")
                            displayMessage.append(itemValue)
                        }
                        "lng" -> {
                            displayMessage.append("<b>")
                            displayMessage.append("Lng")
                            displayMessage.append(":")
                            displayMessage.append("</b> ")
                            displayMessage.append(itemValue)
                        }
                        else -> {
                            val title =
                                jsonObject.getAsJsonObject(Constants.STR_NAME_MAP)
                                    .get(it.key).asString
                            val type =
                                jsonObject.getAsJsonObject(Constants.STR_TYPE_MAP)
                                    .get(it.key).asString

                            if ((type == UI_ELEMENT.FILE.value || type == UI_ELEMENT.SELECT.value) && jsonObject.has(
                                    Constants.LOCAL_KEY
                                )
                            ) {
                                itemValue =
                                    jsonObject.getAsJsonObject(Constants.LOCAL_KEY)
                                        .get(it.key)?.asString
                                        ?: itemValue
                            }

                            displayMessage.append("<b>")
                            displayMessage.append(title)
                            displayMessage.append(":")
                            displayMessage.append("</b> ")
                            when (type) {
                                UI_ELEMENT.DATE.value -> {
                                    val dateValue = itemValue?.parseDate(
                                        DateFormatType.dd_MM_yyyy,
                                        DateFormatType.EEE_MMM_dd_yyyy_HH_mm_ss_zz_zzzz
                                    )
                                    displayMessage.append(dateValue)
                                }
                                UI_ELEMENT.DATETIME.value -> {
                                    try {
                                        val dateValue = itemValue?.parseDate(
                                            DateFormatType.dd_MM_yyyy_HH_mm,
                                            DateFormatType.MMM_dd_yyyy_HH_mm
                                        )
                                        displayMessage.append(dateValue)
                                    } catch (e: Exception) {
                                        val dateValue = itemValue?.parseDate(
                                            DateFormatType.yyyy_mm_dd,
                                            DateFormatType.EEE_MMM_dd_yyyy_HH_mm_ss_zz_zzzz
                                        )
                                        displayMessage.append(dateValue)
                                    }

                                }
                                else -> {
                                    displayMessage.append(itemValue)
                                }
                            }
                        }
                        }

                        displayMessage.append("<br> ")
                    }
                }
            return displayMessage.toString()
        } else {
            return response ?: ""
        }
    }
}