package com.sociograph.davechatbot.ui.chat.adapter
/*

import android.view.View
import com.sociograph.davechatbot.base.BaseRecyclerViewAdapter
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.domain.models.LoadingMessageItem
import com.sociograph.davechatbot.domain.models.MessageItem
import com.sociograph.davechatbot.domain.models.ResponseType
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.interfaces.MessageAdapterListener
import com.sociograph.davechatbot.ui.chat.view_holder.*

class MessageAdapter : BaseRecyclerViewAdapter<MessageItem, BaseViewHolder<MessageItem>>() {


    var messageAdapterListener: MessageAdapterListener? = null

    override fun getRowLayoutId(viewType: Int): Int {
        return viewType
    }

    override fun getViewHolder(view: View, viewType: Int): BaseViewHolder<MessageItem> {
        when (viewType) {
            OutgoingMessageViewHolder.LAYOUT_ID -> {
                return OutgoingMessageViewHolder(view, messageAdapterListener)
            }

            IncomingMessageViewHolder.LAYOUT_ID -> {
                return IncomingMessageViewHolder(view, messageAdapterListener)
            }

            LoadingMessageViewHolder.INCOMING_LAYOUT_ID, LoadingMessageViewHolder.OUTGOING_LAYOUT_ID -> {
                return LoadingMessageViewHolder(view, messageAdapterListener)
            }

            IncomingOptionsMessageViewHolder.LAYOUT_ID -> {
                return IncomingOptionsMessageViewHolder(view, messageAdapterListener)
            }

            IncomingImageMessageViewHolder.LAYOUT_ID -> {
                return IncomingImageMessageViewHolder(view, messageAdapterListener)
            }
            IncomingURLMessageViewHolder.LAYOUT_ID -> {
                return IncomingURLMessageViewHolder(view, messageAdapterListener)
            }
            IncomingVideoMessageViewHolder.LAYOUT_ID -> {
                return IncomingVideoMessageViewHolder(view, messageAdapterListener)
            }
            IncomingFormMessageViewHolder.LAYOUT_ID -> {
                return IncomingFormMessageViewHolder(view, messageAdapterListener)
            }
            IncomingThumbnailsMessageViewHolder.LAYOUT_ID -> {
                return IncomingThumbnailsMessageViewHolder(view, messageAdapterListener)
            }

        }
        return OutgoingMessageViewHolder(view, messageAdapterListener)
    }

    override fun getItemViewType(position: Int): Int {
        when (val messageItem = dataList[position]) {
            is UserRes -> {
                return OutgoingMessageViewHolder.LAYOUT_ID
            }
            is LoadingMessageItem -> {
                return if (messageItem.isIncoming) {
                    LoadingMessageViewHolder.INCOMING_LAYOUT_ID
                } else {
                    LoadingMessageViewHolder.OUTGOING_LAYOUT_ID
                }
            }

            is ConversationRes -> {
                return getIncomingLayoutId(messageItem)
            }
        }

        return super.getItemViewType(position)
    }

    private fun getIncomingLayoutId(conversationItem: ConversationRes): Int {
        return when (conversationItem.data?.responseType) {
            ResponseType.OPTIONS.value -> {
                IncomingOptionsMessageViewHolder.LAYOUT_ID
            }
            ResponseType.FORM.value -> {
                IncomingFormMessageViewHolder.LAYOUT_ID
            }
            ResponseType.IMAGE.value -> {
                IncomingImageMessageViewHolder.LAYOUT_ID
            }
            ResponseType.URL.value -> {
                IncomingURLMessageViewHolder.LAYOUT_ID
            }
            ResponseType.VIDEO.value -> {
                IncomingVideoMessageViewHolder.LAYOUT_ID
            }
            ResponseType.THUMBNAILS.value -> {
                IncomingThumbnailsMessageViewHolder.LAYOUT_ID
            }
            else -> {
                IncomingMessageViewHolder.LAYOUT_ID
            }
        }
    }

    override fun bind(
        viewHolder: BaseViewHolder<MessageItem>,
        position: Int,
        item: MessageItem
    ) {
        viewHolder.bindViewHolder(item)
    }

}*/
