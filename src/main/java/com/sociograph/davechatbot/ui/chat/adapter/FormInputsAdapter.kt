package com.sociograph.davechatbot.ui.chat.adapter


import android.text.InputType
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.gson.JsonObject
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.base.BaseRecyclerViewAdapter
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.base.showDialog
import com.sociograph.davechatbot.core.ConfigData
import com.sociograph.davechatbot.databinding.RFormFileInputViewBinding
import com.sociograph.davechatbot.databinding.RFormGeoLocationBinding
import com.sociograph.davechatbot.databinding.RFormInputViewBinding
import com.sociograph.davechatbot.databinding.RFormSpinnerViewBinding
import com.sociograph.davechatbot.domain.models.UI_ELEMENT
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.FormItem
import com.sociograph.davechatbot.extension.getCapitalizeString
import com.sociograph.davechatbot.extension.openDatePickerDialog
import com.sociograph.davechatbot.extension.openDateTimePickerDialog
import com.sociograph.davechatbot.interfaces.MessageAdapterListener
import com.sociograph.davechatbot.ui.location_selection.LocationSelectionDialog
import com.sociograph.davechatbot.ui.location_selection.LocationSelectionDialogListener
import java.io.File

class FormInputsAdapter(
    private var enableForm: Boolean,
    private var messageAdapterListener: MessageAdapterListener?,
    private var conversationItem: ConversationRes
) :
    BaseRecyclerViewAdapter<FormItem, BaseViewHolder<FormItem>>() {

    companion object {
        const val VIEW_TEXT = 1
        const val VIEW_DATE = 2
        const val VIEW_FILE = 3
        const val VIEW_SELECT = 4
        const val VIEW_GEOLOCATION = 5
    }

    override fun getRowLayoutId(viewType: Int): Int {
        if (viewType == VIEW_TEXT || viewType == VIEW_DATE) {
            return R.layout.r_form_input_view
        } else if (viewType == VIEW_FILE) {
            return R.layout.r_form_file_input_view
        } else if (viewType == VIEW_SELECT) {
            return R.layout.r_form_spinner_view
        } else if (viewType == VIEW_GEOLOCATION) {
            return R.layout.r_form_geo_location
        }
        return R.layout.r_form_input_view
    }

    override fun getItemViewType(position: Int): Int {
        val formItem = dataList[position]
        when (formItem.ui_element) {
            UI_ELEMENT.TEXTAREA.value,
            UI_ELEMENT.TEXT.value,
            UI_ELEMENT.NUMBER.value -> {
                return VIEW_TEXT
            }
            UI_ELEMENT.DATETIME.value,
            UI_ELEMENT.DATE.value -> {
                return VIEW_DATE
            }
            UI_ELEMENT.FILE.value -> {
                return VIEW_FILE
            }
            UI_ELEMENT.SELECT.value -> {
                return VIEW_SELECT
            }
            UI_ELEMENT.GEOLOCATION.value -> {
                return VIEW_GEOLOCATION
            }
        }

        return super.getItemViewType(position)
    }

    override fun getViewHolder(view: View, viewType: Int): BaseViewHolder<FormItem> {
        return when (viewType) {
            VIEW_DATE -> {
                FormDateInputsViewHolder(view)
            }
            VIEW_FILE -> {
                FormFileInputsViewHolder(view)
            }
            VIEW_SELECT -> {
                FormSelectInputsViewHolder(view)
            }
            VIEW_GEOLOCATION -> {
                FormGeoLocationViewHolder(view)
            }
            else -> {
                FormInputsViewHolder(view)
            }
        }

    }

    override fun bind(
        viewHolder: BaseViewHolder<FormItem>,
        position: Int,
        item: FormItem
    ) {
        viewHolder.bindViewHolder(item)
    }

    fun updateFormState(enable: Boolean) {
        enableForm = enable
        notifyDataSetChanged()
    }

    inner class FormInputsViewHolder(itemView: View) :
        BaseViewHolder<FormItem>(itemView) {

        val bind = RFormInputViewBinding.bind(itemView)

        override fun bindViewHolder(item: FormItem) {

            bind.edtInput.hint = item.placeholder
            bind.edtInput.tag = item.value
            bind.edtInput.setText(item.value)
            bind.edtInput.tag = null

            bind.edtInput.isEnabled = enableForm

            when (item.ui_element) {
                UI_ELEMENT.NUMBER.value -> {
                    bind.edtInput.inputType = InputType.TYPE_CLASS_NUMBER
                }
                UI_ELEMENT.TEXT.value -> {
                    bind.edtInput.inputType = InputType.TYPE_CLASS_TEXT
                }
                UI_ELEMENT.TEXTAREA.value -> {
                    bind.edtInput.inputType =
                        InputType.TYPE_CLASS_TEXT or
                                InputType.TYPE_TEXT_FLAG_MULTI_LINE or
                                InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                    bind.edtInput.minLines = 2
                    bind.edtInput.maxLines = 2
                }
            }

            bind.edtInput.addTextChangedListener {
                if (bind.edtInput.tag == null) {
                    item.value = it.toString()
                }
            }
        }
    }

    inner class FormDateInputsViewHolder(itemView: View) :
        BaseViewHolder<FormItem>(itemView) {

        val bind = RFormInputViewBinding.bind(itemView)

        override fun bindViewHolder(item: FormItem) {

            bind.edtInput.hint = item.placeholder
            bind.edtInput.setText(item.value)

            bind.edtInput.isEnabled = enableForm

            when (item.ui_element) {
                UI_ELEMENT.DATETIME.value -> {
                    bind.edtInput.inputType = InputType.TYPE_CLASS_DATETIME
                    bindInputForDateTime(item)
                }
                UI_ELEMENT.DATE.value -> {
                    bind.edtInput.inputType = InputType.TYPE_DATETIME_VARIATION_DATE
                    bindInputForDate(item)
                }
                else -> {
                    bind.edtInput.inputType = InputType.TYPE_CLASS_TEXT
                }
            }
        }

        private fun bindInputForDate(item: FormItem) {
            bind.edtInput.setText(item.value)
            bind.edtInput.keyListener = null
            bind.edtInput.isFocusableInTouchMode = false
            bind.edtInput.setOnClickListener {
                bind.edtInput.openDatePickerDialog(item.minDate, item.maxDate) { dateStr: String ->
                    item.value = dateStr
                }
            }
        }

        private fun bindInputForDateTime(item: FormItem) {
            bind.edtInput.setText(item.value)
            bind.edtInput.keyListener = null
            bind.edtInput.isFocusableInTouchMode = false
            bind.edtInput.setOnClickListener {
                bind.edtInput.openDateTimePickerDialog(
                    item.minDate,
                    item.maxDate,
                ) { dateStr: String ->
                    item.value = dateStr
                }
            }
        }
    }

    inner class FormFileInputsViewHolder(itemView: View) :
        BaseViewHolder<FormItem>(itemView) {

        val bind = RFormFileInputViewBinding.bind(itemView)

        override fun bindViewHolder(item: FormItem) {

            bind.txTitle.text = item.title?.getCapitalizeString()

            if (item.value.isNullOrEmpty().not()) {
                bind.txtFileName.text = File(item.value!!).name
            } else {
                bind.txtFileName.text = getString(R.string.lbl_no_file_chosen)
            }

            bind.btnChooseFile.isEnabled = enableForm
            bind.btnChooseFile.alpha = if (enableForm) 1f else 0.5f
            bind.btnChooseFile.setOnClickListener {
                bind.txtError.apply {
                    text = ""
                    isVisible = false
                }
                messageAdapterListener?.onChooseFileClick(
                    bind,
                    bindingAdapterPosition,
                    conversationItem
                )
            }
        }
    }

    inner class FormSelectInputsViewHolder(itemView: View) :
        BaseViewHolder<FormItem>(itemView) {

        val bind = RFormSpinnerViewBinding.bind(itemView)
        override fun bindViewHolder(item: FormItem) {
            val objects = item.options.map { it.lastOrNull() ?: "" }.toMutableList()

            bind.spinnerSelect.isEnabled = enableForm

            objects.add(0, item.placeholder ?: "")
            bind.spinnerSelect.adapter = ArrayAdapter(
                context,
                android.R.layout.simple_spinner_dropdown_item,
                objects
            )

            val selectedItemPosition = item.options.indexOfFirst { it.first() == item.value }+1
            bind.spinnerSelect.onItemSelectedListener = null
            bind.spinnerSelect.setSelection(selectedItemPosition, false)
            bind.spinnerSelect.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        parent?.selectedItem?.let {
                            val selectedStr = it as String
                            val find = item.options.find { it.last() == selectedStr }
                            item.displayValue = find?.lastOrNull()
                            item.value = find?.firstOrNull()
                            messageAdapterListener?.setFilledConversationData(conversationItem)
                        }

                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                }
        }
    }

    inner class FormGeoLocationViewHolder(itemView: View) :
        BaseViewHolder<FormItem>(itemView) {
        val bind = RFormGeoLocationBinding.bind(itemView)

        override fun bindViewHolder(item: FormItem) {

            bind.edtSelectLocation.isEnabled = enableForm
            bind.edtSelectLocation.keyListener = null
            bind.edtSelectLocation.hint = item.placeholder

            bind.edtSelectLocation.setOnClickListener {
                try {
                    if (ConfigData.GOOGLE_API_KEY.isEmpty()
                        || Places.isInitialized().not()
                    ) return@setOnClickListener
                    messageAdapterListener?.let {
                        val locationSelectionDialog = LocationSelectionDialog()
                        locationSelectionDialog.showDialog(it.getFragmentManager())
                        locationSelectionDialog.listener =
                            object : LocationSelectionDialogListener {
                                override fun onSelectLocation(latLng: LatLng, title: String) {
                                    bind.edtSelectLocation.setText(title)
                                    val jsonObject = JsonObject()
                                    jsonObject.addProperty("lat", latLng.latitude.toString())
                                    jsonObject.addProperty("lng", latLng.longitude.toString())
                                    item.value = jsonObject.toString();
                                }
                            }
                    }
                } catch (e: Exception) {
                    return@setOnClickListener
                }
            }
        }

    }
}