package com.sociograph.davechatbot.ui.feedback

import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewModel
import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.support_domain.schedulers.Scheduler
import com.sociograph.davechatbot.utils.SingleLiveEvent

internal class FeedbackViewModel(
    private val appSettingsDataSource: IAppSettingsDataSource,
    private val appDataSource: IAppDataSource
) : BaseViewModel() {


    val uiSetting: UISetting by lazy {
        appSettingsDataSource.uiSetting
    }

    val submitFeedbackSuccess = SingleLiveEvent<Nothing>()

    override fun subscribe() {

    }

    fun submitFeedback(
        usefulnessRating: String,
        accuracyRating: String,
        feedback: String
    ) {
        appSettingsDataSource.engagementId?.let {
            showProgressDialog()
            addRxCall(
                appDataSource.conversationFeedback(
                    it,
                    usefulnessRating,
                    accuracyRating,
                    feedback
                ).subscribeOn(Scheduler.io())
                    .observeOn(Scheduler.ui()).subscribe({
                        hideProgressDialog()
                        submitFeedbackSuccess.call()
                    }, {
                        hideProgressDialog()
                        showSoftMessage.value = it.message
                    })
            )
        }

    }

}
