package com.sociograph.davechatbot.ui.location_selection

import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewModel
import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource

internal class LocationSelectionViewModel(
    private val appSettingsDataSource: IAppSettingsDataSource,
    private val appDataSource: IAppDataSource,
): BaseViewModel() {
    override fun subscribe() {

    }

    val uiSetting: UISetting by lazy {
        appSettingsDataSource.uiSetting
    }
}