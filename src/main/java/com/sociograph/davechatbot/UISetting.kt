package com.sociograph.davechatbot

import android.graphics.drawable.Drawable

internal class UISetting {
    var buttonColor: Int = -1

    var buttonTextColor: Int = -1

    var backgroundViewColor: Int = -1

    var chatBubbleColor: Int = -1

    var chatBubbleTextColor: Int = -1

    var headerColor: Int = -1

    var sendIcon: Drawable? = null

    var closeIcon: Drawable? = null

    var loaderImage: Drawable? = null

    var botIcon: Drawable? = null

    var chatBotIcon: Drawable? = null

    var chatUserIcon: Drawable? = null

    var optionsText: String = ""

    var titleText: String = ""
}