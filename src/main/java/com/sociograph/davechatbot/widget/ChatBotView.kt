package com.sociograph.davechatbot.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.core.Injection
import com.sociograph.davechatbot.databinding.VChatBotBinding
import com.sociograph.davechatbot.ui.chat.ChatViewActivity


class ChatBotView : FrameLayout {
    private lateinit var binding: VChatBotBinding

    private val uiSettings = Injection.provideAppSettingDataSource().uiSetting

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }


    private fun init(attrs: AttributeSet?) {
        binding = VChatBotBinding.inflate(LayoutInflater.from(context), this, true)

        val attr = context.obtainStyledAttributes(attrs, R.styleable.ChatBotView)

        /*setChatButtonBackgroundResource(
            attr.getResourceId(
                R.styleable.ChatBoatView_backgroundView,
                R.drawable.drw_bg_circle
            )
        )*/

        setBoatIconResource(
            attr.getResourceId(
                R.styleable.ChatBotView_botIcon,
                R.drawable.ic_chat
            )
        )

        /*setButtonColorResource(
            attr.getResourceId(
                R.styleable.ChatBotView_buttonColor,
                R.color.white
            )
        )

        setButtonTextColorResource(
            attr.getResourceId(
                R.styleable.ChatBotView_buttonTextColor,
                R.color.black
            )
        )

        setBackgroundViewColorResource(
            attr.getResourceId(
                R.styleable.ChatBotView_backgroundViewColor,
                R.color.white
            )
        )

        setChatBubbleColorResource(
            attr.getResourceId(
                R.styleable.ChatBotView_chatBubbleColor,
                R.color.colorSecondary
            )
        )

        setChatBubbleTextColorResource(
            attr.getResourceId(
                R.styleable.ChatBotView_chatBubbleTextColor,
                R.color.black
            )
        )

        setHeaderColorResource(
            attr.getResourceId(
                R.styleable.ChatBotView_headerColor,
                R.color.colorPrimary
            )
        )

        setSendIconResource(
            attr.getResourceId(
                R.styleable.ChatBotView_sendIcon,
                R.drawable.ic_send
            )
        )

        setCloseIconResource(
            attr.getResourceId(
                R.styleable.ChatBotView_closeIcon,
                R.drawable.ic_close
            )
        )

        setLoaderImageResource(
            attr.getResourceId(
                R.styleable.ChatBotView_loaderImage,
                R.drawable.ic_chat
            )
        )

        setChatBoatIconResource(
            attr.getResourceId(
                R.styleable.ChatBotView_chatBotIcon,
                R.drawable.ic_dave_icon
            )
        )

        setChatUserIconResource(
            attr.getResourceId(
                R.styleable.ChatBotView_chatUserIcon,
                R.drawable.ic_user_chat_icon
            )
        )

        setOptionsTextResource(
            attr.getResourceId(
                R.styleable.ChatBotView_optionsText,
                R.string.lbl_this_is_option
            )
        )

        setTitleTextResource(
            attr.getResourceId(
                R.styleable.ChatBotView_titleText,
                R.string.lbl_chatbot
            )
        )*/

        binding.root.setOnClickListener { onChatButtonClick(it) }
        attr.recycle()
    }

    private fun onChatButtonClick(view: View) {
        ChatViewActivity.startActivity(context)
    }


    /*private fun setChatButtonBackgroundResource(@DrawableRes resourceId: Int) {
        setChatButtonBackground(ContextCompat.getDrawable(context, resourceId))
    }

    private fun setChatButtonBackground(drawable: Drawable?) {
        drawable?.let {
            binding.root.background = drawable
        }

    }*/

    fun setBoatIconResource(@DrawableRes resourceId: Int) {
        setBoatIcon(ContextCompat.getDrawable(context, resourceId))
    }

    fun setBoatIcon(drawable: Drawable?) {
        drawable?.let {
            binding.img.setImageDrawable(drawable)
        }
        uiSettings.botIcon = drawable
    }

    /*fun setButtonColorResource(@ColorRes resourceId: Int) {
        setButtonColor(ContextCompat.getColor(context, resourceId))
    }


    fun setButtonColor(color: Int) {
        uiSettings.buttonColor = color
    }

    fun setButtonTextColorResource(@ColorRes resourceId: Int) {
        setButtonTextColor(ContextCompat.getColor(context, resourceId))
    }


    fun setButtonTextColor(color: Int) {
        uiSettings.buttonTextColor = color
    }

    fun setBackgroundViewColorResource(@ColorRes resourceId: Int) {
        setBackgroundViewColor(ContextCompat.getColor(context, resourceId))
    }


    fun setBackgroundViewColor(color: Int) {
        uiSettings.backgroundViewColor = color
    }

    fun setChatBubbleColorResource(@ColorRes resourceId: Int) {
        setChatBubbleColor(ContextCompat.getColor(context, resourceId))
    }


    fun setChatBubbleColor(color: Int) {
        uiSettings.chatBubbleColor = color
    }

    fun setChatBubbleTextColorResource(@ColorRes resourceId: Int) {
        setChatBubbleTextColor(ContextCompat.getColor(context, resourceId))
    }


    fun setChatBubbleTextColor(color: Int) {
        uiSettings.chatBubbleTextColor = color
    }

    fun setHeaderColorResource(@ColorRes resourceId: Int) {
        setHeaderColor(ContextCompat.getColor(context, resourceId))
    }


    fun setHeaderColor(color: Int) {
        uiSettings.headerColor = color
    }


    fun setSendIconResource(@DrawableRes resourceId: Int) {
        setSendIcon(ContextCompat.getDrawable(context, resourceId))
    }

    fun setSendIcon(drawable: Drawable?) {
        uiSettings.sendIcon = drawable
    }

    fun setCloseIconResource(@DrawableRes resourceId: Int) {
        setCloseIcon(ContextCompat.getDrawable(context, resourceId))
    }

    fun setCloseIcon(drawable: Drawable?) {
        uiSettings.closeIcon = drawable
    }


    fun setLoaderImageResource(@DrawableRes resourceId: Int) {
        setLoaderImage(ContextCompat.getDrawable(context, resourceId))
    }

    fun setLoaderImage(drawable: Drawable?) {
        uiSettings.loaderImage = drawable
    }

    fun setChatBoatIconResource(@DrawableRes resourceId: Int) {
        setChatBoatIcon(ContextCompat.getDrawable(context, resourceId))
    }

    fun setChatBoatIcon(drawable: Drawable?) {
        uiSettings.chatBotIcon = drawable
    }

    fun setChatUserIconResource(@DrawableRes resourceId: Int) {
        setChatUserIcon(ContextCompat.getDrawable(context, resourceId))
    }

    fun setChatUserIcon(drawable: Drawable?) {
        uiSettings.chatUserIcon = drawable
    }

    fun setOptionsTextResource(@StringRes resourceId: Int) {
        setOptionsText(context.getString(resourceId))
    }

    fun setOptionsText(value: String) {
        uiSettings.optionsText = value
    }

    fun setTitleTextResource(@StringRes resourceId: Int) {
        setTitleText(context.getString(resourceId))
    }

    fun setTitleText(value: String) {
        uiSettings.titleText = value
    }*/

}


/*
* Sample to use this view
* */
/*<!--<com.sociograph.davechatbot.widget.ChatBotView
        android:id="@+id/chatBoatView"
        android:layout_width="80dp"
        android:layout_height="80dp"
        android:layout_gravity="bottom|end"
        android:layout_margin="16dp"
        app:botIcon="@drawable/ic_chat"
        app:buttonColor="@color/green"
        app:buttonTextColor="@color/black"
        app:backgroundViewColor="@color/cyan"
        app:chatBubbleColor="@color/pink"
        app:chatBubbleTextColor="@color/white"
        app:headerColor="@color/purple_200"
        app:closeIcon="@drawable/ic_logout"
        app:chatBotIcon="@drawable/ic_chatbot_icon"
        app:chatUserIcon="@drawable/ic_chat_user_icon"
        app:optionsText="@string/lbl_this_is_options"
        app:titleText="@string/app_name"
        app:loaderImage="@drawable/ic_chat"
        app:sendIcon="@drawable/ic_send_icon"
        android:background="@drawable/drw_bg_circle" />-->*/