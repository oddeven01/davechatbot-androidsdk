package com.sociograph.davechatbot.extension

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.widget.TextView
import com.sociograph.davechatbot.domain.support_domain.ext.DateFormatType
import com.sociograph.davechatbot.domain.support_domain.ext.parseDate
import org.joda.time.DateTime

fun TextView.openDatePickerDialog(
    minDate: String?,
    maxDate: String?,
    onSetDate: (dateStr: String) -> Unit
) {
    val now = DateTime.now()

    val datePickerDialog = DatePickerDialog(
        context,
        { view, year, monthOfYear, dayOfMonth ->

            val dateTime = DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0)
            val parseDate = dateTime.parseDate(DateFormatType.dd_MM_yyyy)
            text = parseDate
            tag = dateTime
            onSetDate(parseDate)
        },
        now.year,
        now.monthOfYear,
        now.dayOfMonth
    )
    minDate?.let {
        datePickerDialog.datePicker.minDate = minDate.parseDate(DateFormatType.yyyy_mm_dd).millis
    }
    maxDate?.let {
        datePickerDialog.datePicker.maxDate = maxDate.parseDate(DateFormatType.yyyy_mm_dd).millis
    }
    datePickerDialog.show()
}


fun TextView.openDateTimePickerDialog(
    minDate: String?,
    maxDate: String?,
    onSetDateTime: (dateStr: String) -> Unit
) {
    val now = DateTime.now()

    val datePickerDialog = DatePickerDialog(
        context,
        { _, year, monthOfYear, dayOfMonth ->
            val timePickerDialog = TimePickerDialog(
                context,
                { _, hourOfDay, minuteOfHour ->
                    val dateTime =
                        DateTime(year, monthOfYear + 1, dayOfMonth, hourOfDay, minuteOfHour)
                    val parseDate = dateTime.parseDate(DateFormatType.dd_MM_yyyy_HH_mm)
                    text = parseDate
                    tag = dateTime
                    onSetDateTime(parseDate)
                },
                now.hourOfDay,
                now.minuteOfHour,
                false
            )
            timePickerDialog.show()
        },
        now.year,
        now.monthOfYear,
        now.dayOfMonth
    )

    minDate?.let {
        datePickerDialog.datePicker.minDate = minDate.parseDate(DateFormatType.yyyy_mm_dd).millis
    }
    maxDate?.let {
        datePickerDialog.datePicker.maxDate = maxDate.parseDate(DateFormatType.yyyy_mm_dd).millis
    }
    datePickerDialog.show()
}