package com.sociograph.davechatbot.extension

import android.content.ContentResolver
import android.net.Uri
import android.provider.OpenableColumns

fun Uri.nameAndLength(contentResolver: ContentResolver)
        : Pair<String, Long> {

    // if "content://" uri scheme, try contentResolver table
    if (scheme.equals(ContentResolver.SCHEME_CONTENT)) {
        return contentResolver.query(
            this,
            arrayOf(OpenableColumns.SIZE, OpenableColumns.DISPLAY_NAME),
            null,
            null,
            null
        )
            ?.use { cursor ->
                // maybe shouldn't trust ContentResolver for size: https://stackoverflow.com/questions/48302972/content-resolver-returns-wrong-size
                val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
                val displayNameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                if (sizeIndex == -1) {
                    return@use Pair("", -1L)
                }
                cursor.moveToFirst()
                return try {
                    Pair(cursor.getString(displayNameIndex), cursor.getLong(sizeIndex))
                } catch (_: Throwable) {
                    Pair("", -1L)
                }
            } ?: Pair("", -1L)
    } else {
        return Pair("", -1L)
    }
}