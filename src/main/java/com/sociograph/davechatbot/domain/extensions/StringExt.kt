package com.sociograph.davechatbot.domain.extensions

import android.util.Log
import com.sociograph.davechatbot.BuildConfig

internal fun String.debug(tag: String = "ChatBoatSDK") {
    if (BuildConfig.DEBUG) {
        Log.d(tag, this)
    }
}

internal fun String.logLargeString(tag: String = "ChatBoatSDK") {
    if (BuildConfig.DEBUG) {
        if (length > 3000) {
            Log.e(tag, substring(0, 3000))
            this.substring(3000).logLargeString()
        } else {
            Log.e(tag, this) // continuation
        }
    }
}
