package com.sociograph.davechatbot.domain.models

class KeywordItem {
    var mixedList: String = ""
    var customerState: String = ""
    var customerResponse: String = ""
    var keywordList: ArrayList<String> = arrayListOf()
}