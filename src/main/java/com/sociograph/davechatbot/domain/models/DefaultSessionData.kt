package com.sociograph.davechatbot.domain.models

class DefaultSessionData {
    var location_dict = ""
    var browser = "native"
    var os = "android"
    var device_type = "mobile"
}