package com.sociograph.davechatbot.domain.models

internal enum class UI_ELEMENT(val value: String) {
    NUMBER("number"),
    TEXT("text"),
    TEXTAREA("textarea"),
    DATETIME("datetime"),
    DATE("date"),
    FILE("file"),
    SELECT("select"),
    GEOLOCATION("geolocation");

    companion object {
        fun findEnumFromValue(lookupValue: String): UI_ELEMENT {
            for (value in values()) {
                if (value.value == lookupValue) {
                    return value
                }
            }
            return TEXT
        }
    }
}
