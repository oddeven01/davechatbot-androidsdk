package com.sociograph.davechatbot.domain.models

internal enum class ResponseType(val value: String) {
    NONE("none"),
    OPTIONS("options"),
    IMAGE("image"),
    FORM("form"),
    URL("url"),
    VIDEO("video"),
    THUMBNAILS("thumbnails");

    companion object {
        fun findEnumFromValue(lookupValue: String): ResponseType {
            for (value in values()) {
                if (value.value == lookupValue) {
                    return value
                }
            }
            return NONE
        }
    }
}
