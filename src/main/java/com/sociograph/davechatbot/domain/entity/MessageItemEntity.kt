package com.sociograph.davechatbot.domain.entity

import androidx.annotation.NonNull
import androidx.recyclerview.widget.DiffUtil
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.sociograph.davechatbot.domain.models.MessageItem
import com.sociograph.davechatbot.domain.support_domain.room.BaseEntity
import com.sociograph.davechatbot.domain.support_domain.room.ResponseConverter

@Entity(
    tableName = MessageItemEntity.TABLE_NAME
)
open class MessageItemEntity : BaseEntity() {

    object Fields {
        const val PK_ID = "id"
        const val RESPONSE = "response"
        const val IS_FROM_ME = "is_from_me"
        const val SEQUENCE = "sequence"
    }

    companion object {
        const val TABLE_NAME = "tbl_message_item"

        var DIFF_CALLBACK: DiffUtil.ItemCallback<MessageItemEntity> =
            object : DiffUtil.ItemCallback<MessageItemEntity>() {
                override fun areItemsTheSame(
                    @NonNull oldItem: MessageItemEntity,
                    @NonNull newItem: MessageItemEntity
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    @NonNull oldItem: MessageItemEntity,
                    @NonNull newItem: MessageItemEntity
                ): Boolean {
                    return oldItem.toString() == newItem.toString()
                }
            }
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = Fields.PK_ID)
    var id: Long = 0

    @TypeConverters(ResponseConverter::class)
    @ColumnInfo(name = Fields.RESPONSE)
    var response: MessageItem? = null

    @ColumnInfo(name = Fields.IS_FROM_ME)
    var isFromMe: Boolean = false

    @ColumnInfo(name = Fields.SEQUENCE)
    var sequence: Int = -1


    override fun equals(other: Any?): Boolean {
        val messageArchiveEntity = other as MessageItemEntity
        return messageArchiveEntity.id == this.id
    }

    override fun toString(): String {
        return "MessageItemEntity(id=$id, response='${response.toString()}', isFromMe=$isFromMe)"
    }


}
