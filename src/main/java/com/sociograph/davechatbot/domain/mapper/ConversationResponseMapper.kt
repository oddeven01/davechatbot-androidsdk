package com.sociograph.davechatbot.domain.mapper

import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.support_domain.ext.DateFormatType
import com.sociograph.davechatbot.domain.support_domain.ext.convertUTCToDefault
import com.sociograph.davechatbot.domain.support_domain.ext.parseDate
import com.sociograph.davechatbot.domain.support_domain.mapper.Mapper
import org.joda.time.DateTime


internal class ConversationResponseMapper : Mapper<ConversationRes, MessageItemEntity>() {
    override fun map(value: ConversationRes): MessageItemEntity {
        val messageItemEntity = MessageItemEntity()
        messageItemEntity.let {
            it.response = value
            it.createAt = value.timestamp?.parseDate(DateFormatType.yyyy_MM_dd_hh_mm_ss_a_Z)
                ?.convertUTCToDefault()
                ?: DateTime.now()
            it.isFromMe = false
            it.sequence= value.sequence
        }

        return messageItemEntity
    }

    override fun reverseMap(value: MessageItemEntity): ConversationRes {
        return value.response as ConversationRes
    }
}