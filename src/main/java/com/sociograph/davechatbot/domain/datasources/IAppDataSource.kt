package com.sociograph.davechatbot.domain.datasources

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.sociograph.davechatbot.domain.datasources.local.ILocalDataSource
import com.sociograph.davechatbot.domain.datasources.remote.IRestDataSource
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.usecase.GetHistoryV1UseCase

internal interface IAppDataSource : IRestDataSource, ILocalDataSource {

    fun getMessageDataSource(getHistoryV1UseCase: GetHistoryV1UseCase): LiveData<PagingData<MessageItemEntity>>
}