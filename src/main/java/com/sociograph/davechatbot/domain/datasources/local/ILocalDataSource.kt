package com.sociograph.davechatbot.domain.datasources.local

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.UserRes

internal interface ILocalDataSource {
    fun insertMessage(userRes: UserRes): Long
    fun getMessageById(id: Long): MessageItemEntity

    fun updateResponseById(id: Long, userRes: UserRes)
    fun getMessageCount(): Int
    fun insertAllMessageItem(messageItemList: List<MessageItemEntity>)
    fun getMessageWithPaging(): LiveData<PagingData<MessageItemEntity>>
    fun insertMessage(conversationRes: ConversationRes): Long
    fun getLastMessageFromSystem(): MessageItemEntity?
    fun clearDatabase()
    fun updateMessageItem(messageItem: MessageItemEntity)
    fun getMessagePagingSource(): PagingSource<Int, MessageItemEntity>
    fun getSystemMessageCount(): Int
    fun getLastMessageFrom(): MessageItemEntity?
}