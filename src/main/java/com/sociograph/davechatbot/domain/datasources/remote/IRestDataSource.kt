package com.sociograph.davechatbot.domain.datasources.remote

import com.google.gson.JsonElement
import com.sociograph.davechatbot.domain.resmodels.*
import io.reactivex.Single

internal interface IRestDataSource {

    fun login(
        userName: String,
        password: String,
        roles: String,
        attrs: List<String>,
        enterPriseId: String
    ): Single<LoginRes>

    fun conversation(
        conversationId: String,
        daveUserId: String,
        customerResponse: String?,
        customerState: String?,
        engagementId: String?,
        systemResponse: String?,
        queryType: String?
    ): Single<ConversationRes>

    fun getConversationHistory(conversationId: String): Single<ConversationHistoryRes>
    fun getConversationHistory(
        conversationId: String,
        pageSize: Int,
        pageNumber: Int
    ): Single<ConversationHistoryRes>

    fun getConversationKeywords(conversationId: String): Single<ConversationKeywordRes>
    fun uploadFile(filePath: String): Single<UploadFileRes>
    fun conversationFeedback(
        engagementId: String,
        usefulnessRating: String,
        accuracyRating: String,
        feedback: String
    ): Single<JsonElement>

    fun conversationFeedbackResponse(
        engagementId: String,
        responseId: String,
        responseRating: String,
    ): Single<JsonElement>
}