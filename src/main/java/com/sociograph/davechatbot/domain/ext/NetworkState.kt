package com.sociograph.davechatbot.domain.ext
class NetworkState(val status: Status, val msg: String? = null) {
    enum class Status {
        RUNNING, SUCCESS, FAILED
    }

    companion object {
        var LOADED: NetworkState = NetworkState(Status.SUCCESS, "Success")
        var LOADING: NetworkState = NetworkState(Status.RUNNING, "Running")
        fun error(msg: String?) = NetworkState(Status.FAILED, msg)
    }

}