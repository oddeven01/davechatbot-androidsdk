package com.sociograph.davechatbot.domain.ext

abstract class PageItem {
    abstract val itemId: String
}