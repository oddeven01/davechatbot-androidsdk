package com.sociograph.davechatbot.domain.support_domain.usecases

internal abstract class UseCase<INPUT_TYPE, OUTPUT_TYPE> {
    abstract fun execute(params: INPUT_TYPE? = null): OUTPUT_TYPE
}