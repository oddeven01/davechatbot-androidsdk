package com.sociograph.davechatbot.domain.support_domain.models

internal class MError : BaseModel() {
    /* {
         "status": "failure",
             "Error": {
                 "Code": "1001",
                 "Name": "ValidationError",
                 "Message": "The parameters passed to the API call are invalid, parameters that caused the problem are \"ic_email\""
             }
     }*/
    var errorType: ErrorType? = null
    var code = ""
    var name = ""
    var message = ""
    var httpCode: Int = 0
    var errorBody: String? = null
}
