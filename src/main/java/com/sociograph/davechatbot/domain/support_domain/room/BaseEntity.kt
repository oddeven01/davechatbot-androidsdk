package com.sociograph.davechatbot.domain.support_domain.room

import androidx.room.ColumnInfo
import androidx.room.TypeConverters
import org.joda.time.DateTime


open class BaseEntity {

    object Fields {
        const val IS_DELETED = "is_deleted"
        const val IS_ACTIVE = "is_active"
        const val CREATE_AT = "create_at"
    }

    @ColumnInfo(name = Fields.IS_ACTIVE)
    var isActive: Boolean = false

    @ColumnInfo(name = Fields.IS_DELETED)
    var isDeleted: Boolean = false

    @TypeConverters(DateConverter::class)
    @ColumnInfo(name = Fields.CREATE_AT)
    var createAt: DateTime = DateTime.now()

}