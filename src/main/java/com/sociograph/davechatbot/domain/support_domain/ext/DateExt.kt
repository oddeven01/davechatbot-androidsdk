package com.sociograph.davechatbot.domain.support_domain.ext

import android.text.format.DateUtils
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.text.SimpleDateFormat
import java.util.*


internal enum class DateFormatType(val value: String) {
    HH_mm("HH:mm"),
    dd_MM_hh_mm_a("dd'/'MM',' hh:mm a"),
    yyyy_MM_dd_T_HH_mm_ss_SSS_Z("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"),
    yyyy_MM_dd_HH_mm_ss("yyyy-MM-dd HH:mm:ss"),
    dd_MM_yyyy_HH_mm("dd/MM/yyyy HH:mm"),
    yyyy_mm_dd("yyyy-MM-dd"),
    dd_MM_yyyy("dd/MM/yyyy"),
    MMM_dd_yyyy_HH_mm("MMM dd, yyyy, HH:mm"),
    EEE_MMM_dd_yyyy_HH_mm_ss_zz_zzzz("EEE MMM dd yyyy HH:mm:ss zz (zzzz)"),
    EEE_dd_MMM_yyyy_HH_mm_ss_z("EEE, dd MMM yyyy HH:mm:ss z"),
    yyyy_MM_dd_hh_mm_ss_a_Z("yyyy-MM-dd hh:mm:ss a Z");

}

internal val hh_mm_a: SimpleDateFormat = SimpleDateFormat("hh:mm a")

internal fun Date?.getMessageTime(): CharSequence? {
    return hh_mm_a.format(this)
}

internal fun DateTime?.getMessageTime(): CharSequence? {
    val forPattern = DateTimeFormat.forPattern(DateFormatType.HH_mm.value)
    return forPattern.print(this)

}

internal fun DateTime?.getCopyMessageTime(): CharSequence? {
    val forPattern = DateTimeFormat.forPattern(DateFormatType.dd_MM_hh_mm_a.value)
    return forPattern.print(this)

}

internal fun DateTime?.getMessageDate(): CharSequence? {
    return DateTimeFormat.mediumDate().print(this)
}

internal fun DateTime?.isToday(): Boolean {
    if (this == null) {
        return false
    } else {
        return DateUtils.isToday(this.millis)
    }
}

internal fun String.parseDate(currentFormatType: DateFormatType, outputFormatType: DateFormatType): String {
    val parse = DateTime.parse(this, DateTimeFormat.forPattern(currentFormatType.value))
    val formatter = DateTimeFormat.forPattern(outputFormatType.value)
    return formatter.print(parse)
}

internal fun String.parseDate(currentFormatType: DateFormatType): DateTime {
    val parse = DateTime.parse(this, DateTimeFormat.forPattern(currentFormatType.value))
    return parse
}

internal fun String.parseDate(currentFormatType: DateTimeFormatter): DateTime {
    val parse = DateTime.parse(this, currentFormatType)
    return parse
}


internal fun DateTime.parseDate(outputFormatType: DateFormatType): String {
    val formatter = DateTimeFormat.forPattern(outputFormatType.value)
    return formatter.print(this)
}

internal fun DateTime.convertDefaultToUTC(): DateTime {
    return this.withZone(DateTimeZone.UTC)
}

internal fun DateTime.convertUTCToDefault(): DateTime {
    return this.withZone(DateTimeZone.getDefault())
}

internal fun Date.convertDefaultToUTC(): Date {
    val dateTime = DateTime(this, DateTimeZone.getDefault())
    val withZone = dateTime.withZone(DateTimeZone.UTC).toLocalDateTime()
    return withZone.toDate()
}

internal fun Date.convertUTCToDefault(): Date {
    val dateTime = DateTime(this, DateTimeZone.UTC)
    val withZone = dateTime.withZone(DateTimeZone.getDefault()).toLocalDateTime()
    return withZone.toDate()
}

internal fun String.getRelativeTimeStringFromPostTime(): String {
    val parse =
        DateTime.parse(this, DateTimeFormat.forPattern(DateFormatType.yyyy_MM_dd_HH_mm_ss.value).withZoneUTC())
    return DateUtils.getRelativeTimeSpanString(parse.millis).toString()
}