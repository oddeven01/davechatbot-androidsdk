package com.sociograph.davechatbot.domain.support_domain.models

enum class ErrorType {
    REST_API_ERROR,
    NETWORK_ERROR,
    DECODING_ERROR
}
