package com.sociograph.davechatbot.domain.support_domain.room


import androidx.room.TypeConverter
import org.joda.time.DateTime

class DateConverter {

    @TypeConverter
    fun toDate(timestamp: Long?): DateTime? {
        return if (timestamp == null) null else DateTime(timestamp)
    }

    @TypeConverter
    fun toTimestamp(date: DateTime?): Long? {
        return date?.millis
    }
}


/*public class TimestampConverter {

    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @TypeConverter
    public static Date fromTimestamp(String value) {
        if (value != null) {
            try {
                TimeZone timeZone = TimeZone.getTimeZone("IST");
                df.setTimeZone(timeZone);
                return df.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            return null;
        }
    }


    @TypeConverter
    public static String dateToTimestamp(Date value) {
        TimeZone timeZone = TimeZone.getTimeZone("IST");
        df.setTimeZone(timeZone);
        return value == null ? null : df.format(value);
    }
}*/