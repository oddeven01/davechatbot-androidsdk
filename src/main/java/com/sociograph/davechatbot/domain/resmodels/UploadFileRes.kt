package com.sociograph.davechatbot.domain.resmodels

import com.google.gson.annotations.SerializedName

class UploadFileRes {

    @SerializedName("filename")
    var filename: String=  ""

    @SerializedName("path")
    var path: String=  ""
}

/*{
  "filename": "42133203_9954_3777_801b_1700e7abdd8e.mp4",
  "path": "https:///static/uploads/dave_expo/42133203_9954_3777_801b_1700e7abdd8e.mp4"
}
*/