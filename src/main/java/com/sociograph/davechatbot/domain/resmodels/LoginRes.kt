package com.sociograph.davechatbot.domain.resmodels

import com.google.gson.annotations.SerializedName

data class LoginRes(

	@field:SerializedName("role")
	val role: String,

	@field:SerializedName("api_key")
	val apiKey: String,

	@field:SerializedName("user_id")
	val userId: String,

	@field:SerializedName("enterprise_id")
	val enterpriseId: String,

	@field:SerializedName("push_token")
	val pushToken: String
)
