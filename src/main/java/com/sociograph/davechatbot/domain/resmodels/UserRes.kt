package com.sociograph.davechatbot.domain.resmodels

import com.google.gson.annotations.SerializedName
import com.sociograph.davechatbot.domain.models.MessageItem

data class UserRes(

	@field:SerializedName("customer_state")
	val customerState: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("response_id")
	var responseId: String? = null,

	@field:SerializedName("user_model")
	val userModel: String? = null,

	@field:SerializedName("customer_response")
	val customerResponse: String? = null,

	@field:SerializedName("direction")
	val direction: String? = null,

	@field:SerializedName("timestamp")
	val timestamp: String? = null,

	@field:SerializedName("sequence")
	var sequence: Int = -1


) : MessageItem {
	override fun toString(): String {
		return "UserRes(customerState=$customerState, userId=$userId, responseId=$responseId, userModel=$userModel, customerResponse=$customerResponse, direction=$direction, timestamp=$timestamp)"
	}
}
