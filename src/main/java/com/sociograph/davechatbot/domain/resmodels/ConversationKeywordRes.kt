package com.sociograph.davechatbot.domain.resmodels

import com.google.gson.annotations.SerializedName
import com.sociograph.davechatbot.domain.models.KeywordItem

class ConversationKeywordRes {

    @field:SerializedName("keywords")
    var keywords: ArrayList<KeywordItem> = arrayListOf()
}