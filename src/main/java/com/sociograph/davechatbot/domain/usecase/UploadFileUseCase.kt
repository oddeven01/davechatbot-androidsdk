package com.sociograph.davechatbot.domain.usecase

import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.resmodels.FormItem
import com.sociograph.davechatbot.domain.support_domain.usecases.UseCase
import io.reactivex.Flowable
import io.reactivex.Single

internal class UploadFileUseCase constructor(
    private val appDataSource: IAppDataSource,
) : UseCase<UploadFileUseCase.Params, Single<List<FormItem>>>() {
    override fun execute(params: Params?): Single<List<FormItem>> {
        val uploadFileParam = params!!

        return Flowable
            .fromIterable(uploadFileParam.formItem.filter { it.value.isNullOrEmpty().not() })
            .parallel()
            .sequential()
            .flatMap { formItem ->
                appDataSource.uploadFile(formItem.value ?: "")
                    .toFlowable().map {
                    formItem.serverPath = it.path
                    formItem
                }
            }.toList()
    }


    class Params(
        val formItem: List<FormItem>
    )
}
