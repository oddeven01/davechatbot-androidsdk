package com.sociograph.davechatbot.domain.usecase

import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.support_domain.usecases.UseCase
import io.reactivex.Completable

internal class DisableFormMessageUseCase constructor(
    private val appDataSource: IAppDataSource,
) : UseCase<DisableFormMessageUseCase.Params, Completable>() {
    override fun execute(params: Params?): Completable {
        val disableFormMessageParam = params!!


        val messageItemEntity = appDataSource.getMessageById(disableFormMessageParam.id)
        messageItemEntity.response?.let {
            if (it is ConversationRes) {
                it.enableForm = false
            }
        }
        appDataSource.updateMessageItem(messageItemEntity)
        return Completable.complete()
    }

    class Params(
        val id: Long,
    )
}
