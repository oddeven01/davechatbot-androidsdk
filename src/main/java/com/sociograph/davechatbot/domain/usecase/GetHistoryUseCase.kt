package com.sociograph.davechatbot.domain.usecase

import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.mapper.ConversationResponseMapper
import com.sociograph.davechatbot.domain.mapper.UserResponseMapper
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.domain.support_domain.usecases.UseCase
import io.reactivex.Completable

internal class GetHistoryUseCase constructor(
    private val appDataSource: IAppDataSource,
    private val appSettingsDataSource: IAppSettingsDataSource
) : UseCase<GetHistoryUseCase.Params, Completable>() {
    override fun execute(params: Params?): Completable {
        val getHistoryParam = params!!


        return conversationHistory(getHistoryParam.conversationId)
    }

    private fun conversationHistory(conversationId: String): Completable {
        if (appDataSource.getMessageCount() != 0) {
            return Completable.complete()
        }
        return Completable.fromSingle(
            appDataSource.getConversationHistory(conversationId).map {
                val messageItemList = arrayListOf<MessageItemEntity>()
                it.history.forEach { messageItem ->
                    if (messageItem is UserRes) {
                        messageItemList.add(UserResponseMapper().map(messageItem))
                    } else if (messageItem is ConversationRes) {
                        messageItem.enableForm = false
                        messageItemList.add(ConversationResponseMapper().map(messageItem))
                    }
                }
                appDataSource.insertAllMessageItem(messageItemList)
            }
        )
    }

    class Params(
        val conversationId: String,
    )
}
