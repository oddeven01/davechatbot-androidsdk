package com.sociograph.davechatbot.domain.usecase

import com.sociograph.davechatbot.core.ConfigData
import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.mapper.ConversationResponseMapper
import com.sociograph.davechatbot.domain.mapper.UserResponseMapper
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.domain.support_domain.usecases.UseCase
import io.reactivex.Single

internal class GetHistoryV1UseCase constructor(
    private val appDataSource: IAppDataSource,
    private val appSettingsDataSource: IAppSettingsDataSource
) : UseCase<GetHistoryV1UseCase.Params, Single<List<MessageItemEntity>>>() {
    override fun execute(params: Params?): Single<List<MessageItemEntity>> {
        val getHistoryParam = params!!
        val conversationId = ConfigData.CONVERSATION_ID
        return conversationHistory(conversationId, getHistoryParam.pageNumber,getHistoryParam.pageSize)
    }

    private fun conversationHistory(
        conversationId: String,
        pageNumber: Int,
        pageSize: Int
    ): Single<List<MessageItemEntity>> {
        return appDataSource.getConversationHistory(conversationId, pageSize, pageNumber).map {
            val messageItemList = arrayListOf<MessageItemEntity>()
            it.history.forEach { messageItem ->
                if (messageItem is UserRes) {
                    messageItemList.add(UserResponseMapper().map(messageItem))
                } else if (messageItem is ConversationRes) {
                    messageItem.enableForm = false
                    messageItemList.add(ConversationResponseMapper().map(messageItem))
                }
            }
            messageItemList
        }

    }

    class Params(
        val pageNumber: Int,
        val pageSize: Int,
    )
}
