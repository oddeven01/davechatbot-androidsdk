package com.sociograph.davechatbot.core

import android.content.Context
import com.google.gson.Gson
import com.sociograph.davechatbot.DaveChatBotSdk
import com.sociograph.davechatbot.data.database.RoomDataSource
import com.sociograph.davechatbot.data.database.RoomManager
import com.sociograph.davechatbot.data.repositaries.AppDataRepository
import com.sociograph.davechatbot.data.repositaries.AppSettingsRepository
import com.sociograph.davechatbot.data.repositaries.local.LocalDataRepository
import com.sociograph.davechatbot.data.repositaries.remote.RestDataRepository
import com.sociograph.davechatbot.data.webservices.IService
import com.sociograph.davechatbot.data.webservices.RestService
import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.datasources.local.ILocalDataSource
import com.sociograph.davechatbot.domain.datasources.remote.IRestDataSource
import com.sociograph.davechatbot.domain.usecase.*

internal object Injection {


    private fun provideRestDataSource(): IRestDataSource {
        return RestDataRepository.getInstance(provideService(), provideAppSettingDataSource())
    }

    private fun provideService(): IService {
        return RestService.getInstance().getIService()
    }

    fun provideContext(): Context {
        return DaveChatBotSdk.appContext
    }

    fun provideAppSettingDataSource(): IAppSettingsDataSource {
        return AppSettingsRepository.getInstance(provideContext(), Gson())
    }

    private fun provideLocalDataSource(): ILocalDataSource {
        return LocalDataRepository.getInstance(
            provideAppSettingDataSource(),
            provideRoomDataSource()
        )
    }

    private fun provideRoomDataSource(): RoomDataSource {
        return RoomManager.getInstance(provideContext())
    }

    fun provideAppDataSource(): IAppDataSource {
        return AppDataRepository.getInstance(
            provideAppSettingDataSource(),
            provideRestDataSource(),
            provideLocalDataSource()
        )
    }

    fun provideSendMessageUseCase(): SendMessageUseCase {
        return SendMessageUseCase(provideAppDataSource(), provideAppSettingDataSource())
    }

    fun provideGetHistoryUseCase(): GetHistoryUseCase {
        return GetHistoryUseCase(provideAppDataSource(), provideAppSettingDataSource())
    }

    fun provideGetHistoryV1UseCase(): GetHistoryV1UseCase {
        return GetHistoryV1UseCase(provideAppDataSource(), provideAppSettingDataSource())
    }

    fun provideDisableFormMessageUseCase(): DisableFormMessageUseCase {
        return DisableFormMessageUseCase(provideAppDataSource())
    }

    fun provideUploadFileUseCase(): UploadFileUseCase {
        return UploadFileUseCase(provideAppDataSource())
    }

}