package com.sociograph.davechatbot.core

import com.sociograph.davechatbot.domain.models.DefaultSessionData

object ConfigData {

    var BASE_API_URL: String = "https://staging.iamdave.ai"

    var SIGNUP_API_KEY: String = ""
    var ENTERPRISE_ID: String = "dave_expo"
    var CONVERSATION_ID: String = ""
    var SPEECH_SERVER: String? = null
    var USER_MODEL_ROLE: String = "person"
    val USER_ID_ATTR: String = "user_id"
    val USER_EMAIL_ATTR: String = "email"
    val USER_PHONE_NUMBER_ATTR: String = "mobile_number"
    var LOGIN_ATTRS: ArrayList<String> = arrayListOf(
        USER_EMAIL_ATTR,
        USER_PHONE_NUMBER_ATTR,
        USER_ID_ATTR
    )
    var DEFAULT_USER_DATA: Map<String, String> = hashMapOf(Pair("person_type", "visitor"))
    var SESSION_MODEL: String = "person_session"
    var SESSION_ID: String = "session_id"
    var SESSION_USER_ID: String = "user_id"
    var DEFAULT_SESSION_DATA: DefaultSessionData = DefaultSessionData()
    var INTERACTION_MODEL: String = "interaction"
    var INTERACTION_USER_ID: String = "person_id"
    var INTERACTION_SESSION_ID: String = "session_id"
    var INTERACTION_STAGE_ATTR: String = "stage"
    var DEFAULT_INTERACTION_DATA: String = ""
    var LANGUAGE: String? = null
    var VOICE_ID: String? = null
    var MAX_SPEECH_DURATION: Long = 10000
    var SESSION_ORIGIN: String? = null
    var INTERACTION_ORIGIN: String? = null
    var AVATAR_ID: String? = null
    var AVATAR_MODEL: String = "avatar"
    var GLB_ATTR: String = "glb_url"
    var EVENT_MODEL: String? = null
    var EVENT_SESSION_ID: String = "session_id"
    var EVENT_USER_ID: String = "user_id"
    var EVENT_DESCRIPTION: String = "event"
    var EVENT_WAIT_TIME: String = "wait_time"
    var EVENT_ORIGIN: String = "origin"
    var DEFAULT_EVENT_DATA: String? = null
    var EVENT_TIMER: String? = null
    var GOOGLE_API_KEY: String = ""
}

