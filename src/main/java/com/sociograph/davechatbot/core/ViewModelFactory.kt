package com.sociograph.davechatbot.core

import android.annotation.SuppressLint
import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.ui.chat.ChatViewModel
import com.sociograph.davechatbot.ui.feedback.FeedbackViewModel
import com.sociograph.davechatbot.ui.location_selection.LocationSelectionViewModel


internal class ViewModelFactory private constructor(
    private val application: Application,
    private val appSettingDataSource: IAppSettingsDataSource,
    private val appDataSource: IAppDataSource,
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(ChatViewModel::class.java) ->
                    ChatViewModel(appSettingDataSource, appDataSource)

                isAssignableFrom(FeedbackViewModel::class.java) ->
                    FeedbackViewModel(appSettingDataSource, appDataSource)

                isAssignableFrom(LocationSelectionViewModel::class.java) ->
                    LocationSelectionViewModel(appSettingDataSource, appDataSource)


                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application) =
            INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                INSTANCE ?: ViewModelFactory(
                    application,
                    Injection.provideAppSettingDataSource(),
                    Injection.provideAppDataSource(),
                )
                    .also { INSTANCE = it }
            }


        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }

    }
}
